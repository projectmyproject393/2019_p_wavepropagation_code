#ifndef MEDUSA_BITS_UTILS_STDTYPESUTILS_HPP_
#define MEDUSA_BITS_UTILS_STDTYPESUTILS_HPP_

/**
 * @file
 * Declaration of utilities for std types.
 */

#include <medusa/Config.hpp>
#include <medusa/bits/utils/assert.hpp>

namespace mm {

/**
 * Splits string by `delim`, returning a vector of tokens (including empty).
 * @code
 * split("a,b,c,", ",");  // returns {"a", "b", "c", ""}
 * @endcode
 */
std::vector<std::string> split(const std::string& s, const std::string& delim);
/// Overload for `char`. @sa split
std::vector<std::string> split(const std::string& s, char delim);

/**
 * Joins a vector of strings back together.
 * @param parts Vector of strings.
 * @param joiner String to glue the parts with.
 */
std::string join(const std::vector<std::string>& parts, const std::string& joiner);
/// Overload for `char`. @sa split
std::string join(const std::vector<std::string>& parts, char joiner);

/// Sorts a container inplace.
template<typename container_t>
container_t& sort(container_t& v) {
    std::sort(v.begin(), v.end());
    return v;
}

/// Sorts a container inplace according to ordering defined by `pred`.
template<typename container_t, typename predicate_t>
container_t& sort(container_t& v, const predicate_t& pred) {
    std::sort(v.begin(), v.end(), pred);
    return v;
}

/// Returns a sorted copy of container.
template<typename container_t>
container_t sorted(container_t v) {
    std::sort(v.begin(), v.end());
    return v;
}

/// Returns a sorted copy of container ordered according to `pred`.
template<typename container_t, typename predicate_t>
container_t sorted(container_t v, const predicate_t& pred) {
    std::sort(v.begin(), v.end(), pred);
    return v;
}

/**
 * Pads a ragged array with given value.
 * Example:
 * @code
 * pad({{1, 2}, {}, {9, 4, 2}}, -1);  // return {{1, 2, -1}, {-1, -1, -1}, {9, 4, 2}}
 * @endcode
 */
template <typename container_t, typename T>
container_t pad(container_t container, T value) {
    decltype(container.begin()->size()) maxsize = 0;
    for (const auto& x : container) {
        if (x.size() > maxsize) {
            maxsize = x.size();
        }
    }
    for (auto& x : container) {
        for (auto i = x.size(); i < maxsize; ++i) x.push_back(value);
    }
    return container;
}

}  // namespace mm

#endif  // MEDUSA_BITS_UTILS_STDTYPESUTILS_HPP_
