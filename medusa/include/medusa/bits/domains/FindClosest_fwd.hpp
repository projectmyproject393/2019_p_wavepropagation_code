#ifndef MEDUSA_BITS_DOMAINS_FINDCLOSEST_FWD_HPP_
#define MEDUSA_BITS_DOMAINS_FINDCLOSEST_FWD_HPP_

/**
 * @file
 * Declarations of FindClosest.
 */

#include <medusa/Config.hpp>
#include <medusa/bits/types/Range_fwd.hpp>

namespace mm {

/**
 * Class representing the engine for finding supports consisting of closest nodes.
 *
 * Usage example:
 * @snippet FindClosest_test.cpp FindClosest usage example
 */
class FindClosest {
  private:
    int support_size;  ///< Support size.
    Range<int> for_which_;  ///< Find support only for these nodes.
    Range<int> search_among_;  ///< Search only among these nodes.
    bool force_self_;  ///< Force each node as the first element of its support.

  public:
    /// Constructs an engine for finding support with given support size.
    FindClosest(int support_size);
    /**
     * Find support only for these nodes. If this option is not used, support is computed for
     * all nodes.
     */
    FindClosest& forNodes(indexes_t for_which);
    /// Search only among given nodes. If this option is not used, all nodes are searched.
    FindClosest& searchAmong(indexes_t search_among);
    /// Put each node as the first of its support, even if it is not in included in searchAmong().
    FindClosest& forceSelf(bool b = true);
    /// Find `num` closest nodes. This methods overrides the value set in the constructor.
    FindClosest& numClosest(int num);

    /// Find support for nodes in domain.
    template <typename domain_t>
    void operator()(domain_t& domain) const;
};

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_FINDCLOSEST_FWD_HPP_
