#ifndef MEDUSA_BITS_DOMAINS_FILLENGINE_HPP_
#define MEDUSA_BITS_DOMAINS_FILLENGINE_HPP_

/**
 * @file
 * Abstract fill engine definition.
 */

#include <medusa/Config.hpp>
#include <medusa/bits/utils/assert.hpp>
#include <functional>

/**
 * @file
 * implementation of FillEngine class.
 */

namespace mm {

template <typename vec_t>
class DomainDiscretization;

/**
 * Base class for fill engines. Subclasses should implement `operator()`
 * which fills given domain with given distribution.
 */
template <typename vec_t>
class FillEngine {
  public:
    typedef DomainDiscretization<vec_t> domain_t;  ///< Domain discretization type.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar type.
    typedef vec_t vector_t;   ///< Vector type.
    /// Store dimension of the domain.
    enum { /** Dimensionality of the domain. */ dim = vec_t::dim };

    /**
     * Runs the fill engine on the selected domain with target density function.
     * @param dr Target density function.
     * @param domain Domain to fill.
     * @param type Type of the added nodes, must be non-positive. If type is 0, the fill engine's
     * default type is used.
     */
    virtual void operator()(domain_t& domain, const std::function<scalar_t(vec_t)>& dr,
                            int type) const = 0;

    /// Overload with default type
    void operator()(domain_t& domain, const std::function<scalar_t(vec_t)>& dr) const {
        this->operator()(domain, dr, 0);
    }

    /// Overload with constant density function.
    virtual void operator()(domain_t& domain, scalar_t r, int type) const {
        this->operator()(domain, [r](const vec_t& /* p */) { return r; }, type);
    }

    /// Overload with default type and constant density function.
    void operator()(domain_t& domain, scalar_t r) const {
        this->operator()(domain, [r](const vec_t& /* p */) { return r; }, 0);
    }
};

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_FILLENGINE_HPP_
