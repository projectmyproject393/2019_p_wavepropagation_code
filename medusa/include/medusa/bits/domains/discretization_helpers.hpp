#ifndef MEDUSA_BITS_DOMAINS_DISCRETIZATION_HELPERS_HPP_
#define MEDUSA_BITS_DOMAINS_DISCRETIZATION_HELPERS_HPP_

#include <medusa/bits/types/Range.hpp>

namespace mm {
namespace discretization_helpers {

/**
 * Returns nodes lying on the line segment `pq` with approximate distances `delta_r`.
 * Boundary point are not included in return set, but are included in the spacing.
 * The last discretization point almost never falls on point q, but over.
 * The points before are squeezed so that the last point coincides with q.
 * @return Discretization points spaced according to the `delta_r` with last and first point
 * (ie. `p` and `q`) excluded.
 */
template <typename vec_t, typename func_t>
Range<vec_t> discretizeLineWithDensity(const vec_t& p, const vec_t& q, const func_t& delta_r) {
    typedef typename vec_t::scalar_t scalar_t;
    scalar_t dist = (q-p).norm();
    assert_msg(dist >= 1e-15, "Points %s and %s are equal.", p, q);
    vec_t dir = (q-p) / dist;
    scalar_t cur_dist = 0;
    Range<scalar_t> dists;
    while (cur_dist < dist) {
        cur_dist += delta_r(p+cur_dist*dir);
        dists.push_back(cur_dist);
    }
    scalar_t shrink = dist / cur_dist;
    if (dists.size() <= 2) return {};
    Range<vec_t> points(dists.size()-1);
    for (int i = 0; i < dists.size()-1; ++i) {
        points[i] = p + shrink*dists[i]*dir;
    }
    return points;
}

}  // namespace discretization_helpers
}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_DISCRETIZATION_HELPERS_HPP_
