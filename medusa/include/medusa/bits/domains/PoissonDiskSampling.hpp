#ifndef MEDUSA_BITS_DOMAINS_POISSONDISKSAMPLING_HPP_
#define MEDUSA_BITS_DOMAINS_POISSONDISKSAMPLING_HPP_

/**
 * @file
 * Implementation of Poisson disk sampling algorithm.
 */

#include "PoissonDiskSampling_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/types/Range.hpp>
#include <random>
#include <deque>
#include <medusa/bits/utils/randutils.hpp>
#include <medusa/bits/kdtree/KDTreeMutable.hpp>

/**
 * @file
 * Implementation of PoissonDiskSampling class.
 */

namespace mm {

template <typename vec_t>
PoissonDiskSampling<vec_t>& PoissonDiskSampling<vec_t>::seed(int seed) {
    assert_msg(seed > -1, "Seed should be > -1");
    rseed = seed;
    return *this;
}

/// @cond
template <typename vec_t>
template <typename func_t>
void PoissonDiskSampling<vec_t>::operator()(typename PoissonDiskSampling<vec_t>::domain_t& domain,
                                            const func_t& delta_r,
                                            int type) const {
    // Pre-compute local distribution for new candidates as if radius were equal to 1.
    Range<vec_t> candidates;
    if (dim == 1) {
        candidates.push_back({-1});
        candidates.push_back({1});
    } else if (dim == 2) {
        int n = 6;  // number of new nodes per iteration
        for (int j = 0; j < n; ++j) {
            double f = 2 * M_PI / n * j;
            vec_t v; v[0] = std::cos(f); v[1] = std::sin(f);
            candidates.push_back(v);
        }
    } else if (dim == 3) {
        int n = 13;  // 4*pi*r^2/r^2
        scalar_t offset = 2.0 / n;
        scalar_t increment = M_PI * (3.0 - std::sqrt(5));
        scalar_t y, r, phi;
        for (int i = 0; i < n; ++i) {
            y = ((i * offset) - 1) + (offset / 2);
            r = std::sqrt(1 - y * y);
            phi = i * increment;
            vec_t v;
            v[0] = std::cos(phi) * r; v[1] = y; v[2] = std::sin(phi) * r;
            candidates.push_back(v);
        }
    } else {
        assert_msg(false, "Only 1D, 2D and 3D Poisson Disk sampling supported.");
    }

    // Tree of all existing nodes, used to check if any are too close.
    KDTreeMutable<vec_t> kd3(domain.positions());

    vec_t lo_bound, hi_bound;
    std::tie(lo_bound, hi_bound) = domain.shape().bbox();
    vec_t selected_node;
    std::mt19937 gen((rseed == -1) ? get_seed() : rseed);
    if (std::isnan(initial_point[0])) {
        // Find a random node within the domain, by generating nodes in its bounding box.
        std::vector<std::uniform_real_distribution<scalar_t>> distributions;
        for (int j = 0; j < dim; ++j) distributions.emplace_back(lo_bound[j], hi_bound[j]);
        double r;
        int count = 0;
        do {
            for (int j = 0; j < dim; ++j) { selected_node[j] = distributions[j](gen); }
            r = delta_r(selected_node);
            if (++count >= 10000) {
                assert_msg(false, "No suitable node in domain could be found after 10000 tries. "
                           "This might happen if domain is very thin or already filled with "
                           "nodes. Try manually supplying the initial point.");
            }
        } while (!domain.shape().contains(selected_node) ||
                (kd3.size() >= 1 && kd3.query(selected_node).second[0] < r*r));
    } else {
        selected_node = initial_point;
        assert_msg(domain.shape().contains(selected_node),
                   "Initial point %s is not contained in the domain.", selected_node);
        assert_msg(std::sqrt(kd3.query(selected_node).second[0]) > delta_r(selected_node),
                   "Initial point %s is too close to already existing nodes. Requested distance "
                   "is delta_r = %f, but actual distance is %f.", selected_node,
                   delta_r(selected_node), std::sqrt(kd3.query(selected_node).second[0]));
    }

    Range<vec_t> new_nodes = {selected_node};
    kd3.insert(selected_node);
    std::deque<size_t> to_expand = {0};  // list of nodes to be expanded

    bool alter = false;  // temp variable
    // iteration loop
    for (int c = 0; c < num_iterations && !to_expand.empty(); ++c) {
        size_t node_i = to_expand.front();
        to_expand.pop_front();
        // filter candidates regarding the domain and proximity criteria
        for (auto f : candidates) {
            // target radius,
            scalar_t radius = delta_r(new_nodes[node_i]);
            assert_msg(radius > 0, "Poisson Disk Sampling:: Target radius should be > 0");
            // compute candidate node from precomputed values
            vec_t node = new_nodes[node_i] + f * radius;

            if (!domain.shape().contains(node)) continue;
            Range<double> d = kd3.query(node).second;
            if (d[0] < radius * radius * proximity_factor * proximity_factor)
                continue;

            new_nodes.push_back(node);
            // if we use MC add new to_expand index on a random position
            if (randomize_during_iteration && !to_expand.empty()) {
                // TODO(jureslak): this is super inefective, it can be done better
                std::uniform_int_distribution<> dis(0, to_expand.size() - 1);
                to_expand.insert(to_expand.begin() + dis(gen), new_nodes.size() - 1);
            } else {
                // cheap trick to increase randomness by alternating
                // push front and push back.
                if (alter) to_expand.push_back(new_nodes.size() - 1);
                else to_expand.push_front(new_nodes.size() - 1);
                alter = !alter;
            }
            kd3.insert(node);
        }
    }
    // Add nodes to the domain as internal nodes.
    if (type == 0) type = 1;
    for (const auto& node : new_nodes) domain.addInternalNode(node, type);
}
/// @endcond

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_POISSONDISKSAMPLING_HPP_
