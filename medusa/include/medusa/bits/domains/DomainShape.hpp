#ifndef MEDUSA_BITS_DOMAINS_DOMAINSHAPE_HPP_
#define MEDUSA_BITS_DOMAINS_DOMAINSHAPE_HPP_

/**
 * @file
 * Implementations of base class for domain shapes.
 */

#include "DomainShape_fwd.hpp"
#include "ShapeDifference.hpp"
#include "ShapeUnion.hpp"
#include "FillEngine.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/utils/numutils.hpp>
#include <cmath>

namespace mm {

template <typename vec_t>
std::pair<bool, vec_t> DomainShape<vec_t>::projectPointToBoundary(
        const vec_t& point, vec_t unit_normal) const {
    assert_msg(unit_normal.norm() > 1e-9, "Normal %s given for point %s is zero.",
               unit_normal, point);
    if (dim == 1) return {false, point};
    // Find point on the other side of the boundary with exponential search
    vec_t start = point;
    bool is_inside = contains(start);
    scalar_t max_stretch = 100 * margin_;
    while (true) {
        if (contains(start + max_stretch * unit_normal) != is_inside) break;
        if (contains(start - max_stretch * unit_normal) != is_inside) {
            max_stretch *= -1;
            break;
        }
        max_stretch *= 2;
        if (std::isinf(max_stretch)) {  // hint is bad
            return {false, vec_t()};
        }
    }

    // Find the point on the boundary using bisection
    scalar_t stretch = max_stretch;
    while (std::abs(stretch) > margin_) {
        stretch /= 2;
        if (contains(start + stretch * unit_normal) == is_inside) {
            start += stretch * unit_normal;
        }
    }

    // Make unit normal point outside
    if (is_inside) {
        unit_normal *= signum(max_stretch);
    } else {
        unit_normal *= -signum(max_stretch);
    }

    // Make sure point is inside
    while (!contains(start)) start -= margin_ * unit_normal;

    return {true, start};
}

template <typename vec_t>
ShapeUnion<vec_t> DomainShape<vec_t>::add(const DomainShape& other) const {
    return ShapeUnion<vec_t>(*this, other);
}

template <typename vec_t>
ShapeDifference<vec_t> DomainShape<vec_t>::subtract(const DomainShape& other) const {
    return ShapeDifference<vec_t>(*this, other);
}

template <typename vec_t>
DomainDiscretization<vec_t>
DomainShape<vec_t>::discretizeWithDensity(const std::function<scalar_t(vec_t)>& dr,
                                          FillEngine<vec_t>* fill, int internal_type,
                                          int boundary_type) const {
    auto domain = discretizeBoundaryWithDensity(dr, boundary_type);
    assert_msg(fill != nullptr, "Fill engine must be specified to use this function.");
    fill->operator()(domain, dr, internal_type);
    return domain;
}

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_DOMAINSHAPE_HPP_
