#ifndef MEDUSA_BITS_DOMAINS_DOMAINDISCRETIZATION_HPP_
#define MEDUSA_BITS_DOMAINS_DOMAINDISCRETIZATION_HPP_

/**
 * @file
 * Implementation of discretized domains.
 */

#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/kdtree/KDTree.hpp>
#include <cmath>
#include "DomainDiscretization_fwd.hpp"
#include "DomainShape.hpp"
#include "ShapeUnion.hpp"
#include "ShapeDifference.hpp"
#include "UnknownShape.hpp"

namespace mm {

template <class vec_t>
Range<int> DomainDiscretization<vec_t>::supportSizes() const {
    int N = size();
    Range<int> sizes(N);
    for (int i = 0; i < N; ++i) {
        sizes[i] = support_[i].size();
    }
    return sizes;
}

template <class vec_t>
vec_t& DomainDiscretization<vec_t>::normal(int i) {
    assert_msg(0 <= i && i <= size(), "Index %d out of range [0, %d)", i, size());
    assert_msg(types_[i] < 0, "Node %d must be a boundary node, got type %d.", i, types_[i]);
    assert_msg(boundary_map_[i] != -1, "Node %d does not have a normal. Maybe you manually set"
            " supports and positions instead of using addInternalNode* methods?", i);
    return normals_[boundary_map_[i]];
}

template <class vec_t>
const vec_t& DomainDiscretization<vec_t>::normal(int i) const {
    assert_msg(0 <= i && i <= size(), "Index %d out of range [0, %d)", i, size());
    assert_msg(types_[i] < 0, "Node %d must be a boundary node, got type %d.", i, types_[i]);
    assert_msg(boundary_map_[i] != -1, "Node %d does not have a normal. Maybe you manually set"
            " supports and positions instead of using addInternalNode* methods?", i);
    return normals_[boundary_map_[i]];
}

template <class vec_t>
void DomainDiscretization<vec_t>::addInternalNode(const vec_t& point, int type) {
    assert_msg(type > 0, "This function of for adding internal points, use addBoundaryNode to "
                         "add boundary nodes.", type);
    positions_.push_back(point);
    types_.push_back(type);
    support_.emplace_back();
    boundary_map_.push_back(-1);
}

template <class vec_t>
void DomainDiscretization<vec_t>::addBoundaryNode(
        const vec_t& point, int type, const vec_t& normal) {
    assert_msg(type < 0, "Type of boundary points must be negative, got %d.", type);
    positions_.push_back(point);
    types_.push_back(type);
    support_.emplace_back();
    boundary_map_.push_back(normals_.size());
    normals_.push_back(normal);
}

template <class vec_t>
void DomainDiscretization<vec_t>::changeToBoundary(int i, const vec_t& point, int type,
                                                   const vec_t& normal)  {
    assert_msg(0 <= i && i < size(), "Index %d out of range [0, %d).", i, size());
    assert_msg(types_[i] >= 0, "Point %d is already a boundary point with type %d.", i,
               types_[i]);
    assert_msg(type, "New type must be negative, got %d.", type);
    assert_msg(shape_->contains(point), "Point %s with type %d you are trying to add is not "
            "contained in the domain!", point, type);
    positions_[i] = point;
    types_[i] = type;
    boundary_map_[i] = normals_.size();
    normals_.push_back(normal);
}

template <class vec_t>
void DomainDiscretization<vec_t>::addNodes(const DomainDiscretization<vec_t>& d) {
    for (int i = 0; i < d.size(); ++i) {
        if (d.type(i) < 0) {
            addBoundaryNode(d.pos(i), d.type(i), d.normal(i));
        } else {
            addInternalNode(d.pos(i), d.type(i));
        }
    }
}

template <class vec_t>
void DomainDiscretization<vec_t>::removeNodes(const Range<int>& to_remove)  {
    int n = size();
    Range<bool> remove_mask(n, false);
    remove_mask[to_remove] = true;
    Range<int> remove_normals;
    for (int i = 0; i < n; ++i) {
        if (boundary_map_[i] >= 0 && remove_mask[i]) {  // boundary node that has to be removed
            remove_normals.push_back(boundary_map_[i]);
        } else if (boundary_map_[i] >= 0) {
            boundary_map_[i] -= remove_normals.size();
        }
    }
    normals_.remove(remove_normals);
    boundary_map_.remove(to_remove);
    types_.remove(to_remove);
    positions_.remove(to_remove);
    if (support_.size() == n) support_.remove(to_remove);
}

template <class vec_t>
void DomainDiscretization<vec_t>::clear() {
    positions_.clear();
    types_.clear();
    support_.clear();
    normals_.clear();
    boundary_map_.clear();
}

template <class vec_t>
bool DomainDiscretization<vec_t>::valid() const  {
    int N = size();
    assert_msg(N == types_.size(), "Number of node type entries (%d) not equal to domain size "
            "(%d).", types_.size(), N);
    assert_msg(N == boundary_map_.size(), "Number of boundary map entries (%d) not equal to domain"
            " size (%d).", types_.size(), N);
    assert_msg(N == support_.size(), "Number of node supports (%d) not equal to domain size "
            "(%d).", types_.size(), N);
    assert_msg(N == positions_.size(), "Number of node positions (%d) not equal to domain size "
            "(%d).", types_.size(), N);
    int num_bnd = 0;
    for (int i = 0; i < N; ++i) {
        if (types_[i] < 0) {
            assert_msg(0 <= boundary_map_[i] && boundary_map_[i] < normals_.size(),
                       "Boundary map index %d of node %d must lie in interval [0, %d).",
                       boundary_map_[i], i, normals_.size());
            ++num_bnd;
        } else if (types_[i] > 0) {
            assert_msg(boundary_map_[i] == -1, "Expected boundary map of internal node %d "
                    "to be -1, got %d.", i, boundary_map_[i]);
        }
        assert_msg(shape_->contains(pos(i)), "Position %s at index %d is not contained "
                "in the domain.", pos(i), i);
    }
    assert_msg(num_bnd == normals_.size(), "Number of boundary nodes not the same as number "
            "of normals, got %d nodes with negative type, but %d normals.",
               num_bnd, normals_.size());
    return true;
}

template <class vec_t>
std::ostream& DomainDiscretization<vec_t>::output_report(std::ostream& os) const  {
    int internal_count = (types_ > 0).size();
    int boundary_count = (types_ < 0).size();
    std::size_t mem = sizeof(*this);
    std::size_t mem_pos = mem_used(positions_);
    std::size_t mem_types = mem_used(types_);
    std::size_t mem_supp = mem_used(support_);
    int max_support_size = -1;
    for (int i = 0; i < support_.size(); ++i) {
        max_support_size = std::max(max_support_size, support_[i].size());
        mem_supp += mem_used(support_[i]);
    }
    std::size_t mem_bnd_map = mem_used(boundary_map_);
    std::size_t mem_normals = mem_used(normals_);
    std::size_t mem_total = mem + mem_pos + mem_types + mem_supp + mem_bnd_map + mem_normals;
    os << "    dimension: " << dim << '\n'
       << "    shape: " << *shape_ << '\n'
       << "    number of points: " << size() << " of which " << internal_count
       << " internal and " << boundary_count << " boundary\n"
       << "    max support size: ";
    if (max_support_size == -1) os << "support not found yet";
    else os << max_support_size;
    os << "\n    mem used total: " << mem2str(mem_total) << '\n'
       << "        pos & types:   " << mem2str(mem_pos + mem_types) << '\n'
       << "        supp:   " << mem2str(mem_supp) << '\n'
       << "        bnd & normals: " << mem2str(mem_bnd_map + mem_normals) << '\n';
    return os;
}

template <class vec_t>
DomainDiscretization<vec_t>::DomainDiscretization(const DomainShape<vec_t>& shape) :
        shape_(shape) {}

template <class vec_t>
DomainDiscretization<vec_t>&
DomainDiscretization<vec_t>::add(const DomainDiscretization<vec_t>& d) {
    /*
     * Domain 1 is left almost as-is. Nodes from domain 2, that are not included in d1
     * are added. They are added only if they are not too close to nodes from d1.
     * Additionally, nodes from d1 that are on the portion of the boundary that is included in d2
     * are changed to internal nodes.
     */

    deep_copy_unique_ptr<DomainShape<vec_t>> old_shape(shape());
    shape_ = shape() + d.shape();
    Range<int> bnd = boundary();
    assert_msg(bnd.size() >= 2, "At least two nodes required on the boundary of domain 1.");
    KDTree<vec_t> tree(positions_[bnd]);
    Range<int> idx;
    Range<double> dist;
    for (int i = 0; i < d.size(); ++i) {
        if (!old_shape->contains(d.pos(i))) {
            std::tie(idx, dist) = tree.query(d.pos(i));
            double dx = std::sqrt(tree.query(pos(bnd[idx[0]]), 2).second[1]);
            if (std::sqrt(dist[0]) > 0.9*dx) {
                if (d.type(i) > 0) {
                    addInternalNode(d.pos(i), d.type(i));
                } else {
                    addBoundaryNode(d.pos(i), d.type(i), d.normal(i));
                }
            }
        }
    }

    old_shape = d.shape();
    old_shape->setMargin(-old_shape->margin());
    Range<int> to_change;
    for (int i = 0; i < size(); ++i) {
        if (type(i) < 0 && old_shape->contains(pos(i))) {
            type(i) = -type(i);  // change to internal
            to_change.push_back(i);
        }
    }
    normals_.remove(boundary_map_[to_change].asRange());
    boundary_map_.remove(to_change);
    return *this;
}

template <class vec_t>
DomainDiscretization<vec_t>& DomainDiscretization<vec_t>::subtract(const DomainDiscretization& d) {
    /*
     * Removes given domain d2 from current domain d1. Nodes in d1 that are contained
     * in d2 or less than dx away from d2 are removed. Boundary nodes from d2 that are contained
     * in d1 are added.
     */

    Range<int> bnd = d.boundary();
    assert_msg(bnd.size() >= 2, "At least two nodes are needed on boundary of the second domain.");
    Range<int> to_add;
    for (int i = 0; i < d.size(); ++i) {
        if (d.type(i) < 0 && shape().contains(d.pos(i))) {
            to_add.push_back(i);
        }
    }
    KDTree<vec_t> tree(d.positions_[to_add]);

    Range<int> to_remove;
    Range<int> idx;
    Range<double> dist;
    for (int i = 0; i < size(); ++i) {
        if (d.shape().contains(pos(i))) {
            to_remove.push_back(i);
            continue;
        }
        std::tie(idx, dist) = tree.query(pos(i));
        double dx = std::sqrt(tree.query(d.pos(to_add[idx[0]]), 2).second[1]);
        if (std::sqrt(dist[0]) < 0.9*dx) {
            to_remove.push_back(i);
        }
    }
    removeNodes(std::move(to_remove));

    for (int i : to_add) {
        addBoundaryNode(d.pos(i), d.type(i), -d.normal(i));
    }
    shape_ = shape() - d.shape();
    return *this;
}

template <typename vec_t>
class UnknownShape;

template <class vec_t>
template <typename hdf5_file>
DomainDiscretization<vec_t> DomainDiscretization<vec_t>::load(hdf5_file& file,
                                                              const std::string& name)  {
    file.openGroup(name);
    auto pos = file.readDouble2DArray("pos");
    int n = pos.size();
    UnknownShape<vec_t> shape;
    DomainDiscretization<vec_t> domain(shape);
    domain.positions_.resize(n);
    vec_t low = 1e100;
    vec_t high = -1e100;
    for (int i = 0; i < n; ++i) {
        assert_msg(pos[i].size() == dim, "Node %d has invalid number of coordinates: %s, "
                                         "expected %d.", i, pos[i].size(), dim);
        for (int j = 0; j < dim; ++j) {
            domain.positions_[i][j] = pos[i][j];
            if (pos[i][j] <= low[j]) low[j] = pos[i][j];
            if (pos[i][j] >= high[j]) high[j] = pos[i][j];
        }
    }
    domain.types_ = file.readIntArray("types");
    int bnd_count = (domain.types_ < 0).size();
    domain.boundary_map_ = file.readIntArray("bmap");
    assert_msg((domain.boundary_map_ == -1).size() == n - bnd_count,
               "Number of nodes with boundary index (%d) in bmap is not the same as number "
               "of nodes with negative type (%d).",
               (domain.boundary_map_ == -1).size(), n - bnd_count);
    auto normals = file.readDouble2DArray("normals");
    assert_msg(normals.size() == bnd_count, "Number of normals (%d) must be equal to number of "
                                            "boundary nodes (%d).", normals.size(), bnd_count);
    domain.normals_.resize(normals.size());
    for (int i = 0; i < bnd_count; ++i) {
        assert_msg(normals[i].size() == dim, "Normal %d has invalid number of coordinates: %s, "
                                             "expected %d.", i, normals[i].size(), dim);
        for (int j = 0; j < dim; ++j) {
            domain.normals_[i][j] = normals[i][j];
        }
    }
    // boundary map consistency check
    for (int i = 0; i < n; ++i) {
        if (domain.types_[i] < 0) {
            assert_msg(0 <= domain.boundary_map_[i] && domain.boundary_map_[i] < bnd_count,
                       "Normals list and boundary map inconsistent. Node %d with type %d "
                       "has boundary index %d, but the list of normals is only %d long.",
                       i, domain.types_[i], domain.boundary_map_[i], bnd_count);
        } else {
            assert_msg(domain.boundary_map_[i] == -1,
                       "Boundary map assigned a normal to an internal node %d with type %d, "
                       "bmap[%d] = %d, but should be -1.",
                       i, domain.types_[i], i, domain.boundary_map_[i]);
        }
    }
    domain.support_.resize(n);
    return domain;
}

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_DOMAINDISCRETIZATION_HPP_
