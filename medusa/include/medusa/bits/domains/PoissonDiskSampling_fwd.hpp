#ifndef MEDUSA_BITS_DOMAINS_POISSONDISKSAMPLING_FWD_HPP_
#define MEDUSA_BITS_DOMAINS_POISSONDISKSAMPLING_FWD_HPP_

/**
 * @file
 * Declaration of Poisson disk sampling algorithm.
 */

#include <medusa/Config.hpp>
#include "FillEngine.hpp"

/**
 * @file
 * Declaration of PoissonDiskSampling class.
 */

namespace mm {

template <typename vec_t>
class DomainDiscretization;

/**
 * Poisson Disk Sampling based fill algorithm. The algorithm starts in random node and adds
 * nodes around it on a circle with radius defined in a supplied density function.
 * In each next iteration it selects one of the previously added nodes and repeats the adding.
 * The main bottleneck right now is a search structure. First step in optimization
 * would be better tree bookkeeping, i.e. determine which nodes cannot be in a way anymore
 * this might become tricky with variable nodal densities.
 *
 * Usage example:
 * @snippet PoissonDiskSampling_test.cpp PDS usage example
 */
template <typename vec_t>
class PoissonDiskSampling : public FillEngine<vec_t> {
    int num_iterations = 5000000;  ///< Max number of iterations performed.
    double proximity_factor = 0.95;  ///< Threshold for proximity cutoff.
    bool randomize_during_iteration = true;  ///< Use MC for selection of expanding nodes.
    int rseed = -1;  ///<  User supplied seed if -1 use generate std::random_device.
    vec_t initial_point = NaN;  ///< Point from which the algorithm will start.

  public:
    typedef typename FillEngine<vec_t>::domain_t domain_t;  ///< Domain discretization type.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar type;
    typedef vec_t vector_t;  ///< Vector type.
    using FillEngine<vec_t>::dim;

    PoissonDiskSampling() = default;

    /**
     * Sets maximal number of iterations.
     * @param iterations maximal allowed number of iteration before exiting
     */
    PoissonDiskSampling& iterations(int iterations) {
        num_iterations = iterations; return *this;
    }

    /// Set random seed.
    PoissonDiskSampling& seed(int seed);

    /// Set initial point from which the algorithm will spread the points.
    PoissonDiskSampling& initialPoint(const vec_t& point) {
        initial_point = point; return *this;
    }

    /**
     * Sets proximity relaxation that defines minimal distance between nodes,
     * in other words, nodes closer then `target_radius*proximity_relaxation`
     * will be deleted.
     */
    PoissonDiskSampling& proximityFactor(double proximity_tolerance_factor) {
        proximity_factor = proximity_tolerance_factor; return *this;
    }

    /**
     * If set, selects the next expanded node randomly. Using randomization
     * results in slower but better algorithm, especially in variable density situations.
     */
    PoissonDiskSampling& randomize(bool b) {
        randomize_during_iteration = b; return *this;
    }

    void operator()(domain_t& domain, const std::function<scalar_t(vec_t)>& dr,
                    int type) const override {
        this->template operator()<decltype(dr)>(domain, dr, type);
    }

    /// Fills given domain with distribution `delta_r` starting at `initial_point`.
    template <typename func_t>
    void operator()(domain_t& domain, const func_t& delta_r, int type) const;

    using FillEngine<vec_t>::operator();
};

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_POISSONDISKSAMPLING_FWD_HPP_
