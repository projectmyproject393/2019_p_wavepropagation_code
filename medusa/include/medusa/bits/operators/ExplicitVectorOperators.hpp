#ifndef MEDUSA_BITS_OPERATORS_EXPLICITVECTOROPERATORS_HPP_
#define MEDUSA_BITS_OPERATORS_EXPLICITVECTOROPERATORS_HPP_

/**
 * @file
 * Explicit vector operators implementation.
 */

#include "ExplicitVectorOperators_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <Eigen/Core>
#include "UniformShapeStorage_fwd.hpp"
#include "RaggedShapeStorage_fwd.hpp"

namespace mm {

/**
 * Performs all asserts for a given node: asserts that node index is valid and that shape functions
 * were initialized for a given node.
 * @param node Node index to consider.
 */
#define NODE_ASSERTS(node) \
    assert_msg(0 <= (node) && (node) < ss->size(), "Node index %d must be in range " \
               "[0, num_nodes = %d).", ss->size());

/// @cond
template <class shape_storage_type>
template <class vector_field_t>
Eigen::Matrix<
        typename ExplicitVectorOperators<shape_storage_type>::scalar_t,
        ExplicitVectorOperators<shape_storage_type>::dim,
        ExplicitVectorOperators<shape_storage_type>::dim>
ExplicitVectorOperators<shape_storage_type>::grad(const vector_field_t& u, int node) const {
    NODE_ASSERTS(node);
    Eigen::Matrix<scalar_t, dim, dim> ret;
    for (int d = 0; d < dim; ++d) {
        for (int var = 0; var < dim; ++var) {  // loop over dimensions
            ret(d, var) = ss->d1(var, node, 0) * u[ss->support(node, 0)][d];
            for (int i = 1; i < ss->supportSize(node); ++i) {
                ret(d, var) += ss->d1(var, node, i) * u[ss->support(node, i)][d];
            }
        }
    }
    return ret;
}

template <class shape_storage_type>
template <class vector_field_t>
typename ExplicitVectorOperators<shape_storage_type>::vector_t
ExplicitVectorOperators<shape_storage_type>::lap(const vector_field_t& u, int node) const {
    NODE_ASSERTS(node);
    vector_t lap = static_cast<scalar_t>(0.);
    for (int i = 0; i < ss->supportSize(node); ++i) {
        for (int d = 0; d < dim; ++d) {
            lap[d] += ss->laplace(node, i) * u[ss->support(node, i)][d];
        }
    }
    return lap;
}

template <class shape_storage_type>
template <class vector_field_t>
typename ExplicitVectorOperators<shape_storage_type>::scalar_t
ExplicitVectorOperators<shape_storage_type>::div(const vector_field_t& u, int node) const {
    NODE_ASSERTS(node);
    scalar_t ret = 0;
    for (int var = 0; var < dim; ++var)
        for (int i = 0; i < ss->supportSize(node); ++i)
            ret += ss->d1(var, node, i) * u[ss->support(node, i)][var];
    return ret;
}

template <class shape_storage_type>
template <class vector_field_t>
typename ExplicitVectorOperators<shape_storage_type>::vector_t
ExplicitVectorOperators<shape_storage_type>::graddiv(const vector_field_t& u, int node) const {
    NODE_ASSERTS(node);
    vector_t ret = 0.0;
    for (int d1 = 0; d1 < dim; ++d1) {
        for (int d2 = 0; d2 < dim; ++d2) {  // loop over dimensions
            int dmin = std::min(d1, d2);
            int dmax = std::max(d1, d2);
            for (int i = 0; i < ss->supportSize(node); ++i) {
                ret[d1] += ss->d2(dmin, dmax, node, i) * u[ss->support(node, i)][d2];
            }
        }
    }
    return ret;
}

template <class shape_storage_type>
template <class vector_field_t>
typename ExplicitVectorOperators<shape_storage_type>::vector_t
ExplicitVectorOperators<shape_storage_type>::neumann(const vector_field_t& u, int node,
                                                     const vector_t& normal,
                                                     const vector_t& val) const {
    NODE_ASSERTS(node);
    assert_msg(ss->support(node, 0) == node, "First support node should be the node itself.");
    vector_t result = val;
    scalar_t denominator = 0;
    for (int d = 0; d < dim; ++d) {
        for (int i = 1; i < ss->supportSize(node); ++i) {
            result -= normal[d] * ss->d1(d, node, i) * u[ss->support(node, i)];
        }
        // i = 0
        denominator += normal[d] * ss->d1(d, node, 0);
    }
    assert_msg(std::abs(denominator) >= 1e-14,
               "Node %d has no effect on the flux in direction %s. The cause of this might be wrong"
               " normal direction, bad neighbourhood choice or bad nodal positions.", node, normal);
    return result / denominator;
}
/// @endcond

/// Output basic info about given operators.
template <typename S>
std::ostream& operator<<(std::ostream& os, const ExplicitVectorOperators<S>& op) {
    if (!op.hasShapes()) {
        return os << "Explicit vector operators without any linked storage.";
    }
    return os << "Explicit vector operators over storage: " << *op.ss;
}

template <typename vec_t, sh::shape_flags shape_mask>
ExplicitVectorOperators<UniformShapeStorage<vec_t, shape_mask>>
UniformShapeStorage<vec_t, shape_mask>::explicitVectorOperators() const {
    return ExplicitVectorOperators<UniformShapeStorage<vec_t, shape_mask>>(*this);
}

template <typename vec_t, sh::shape_flags shape_mask>
ExplicitVectorOperators<RaggedShapeStorage<vec_t, shape_mask>>
RaggedShapeStorage<vec_t, shape_mask>::explicitVectorOperators() const {
    return ExplicitVectorOperators<RaggedShapeStorage<vec_t, shape_mask>>(*this);
}

}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_EXPLICITVECTOROPERATORS_HPP_
