#ifndef MEDUSA_BITS_OPERATORS_UNIFORMSHAPESTORAGE_FWD_HPP_
#define MEDUSA_BITS_OPERATORS_UNIFORMSHAPESTORAGE_FWD_HPP_

/**
 * @file
 * Declaration of shape storage for uniformly long shapes.
 */

#include <medusa/Config.hpp>
#include <medusa/bits/types/Range_fwd.hpp>
#include <medusa/bits/operators/shape_flags.hpp>
#include <iosfwd>
#include <Eigen/Core>

namespace mm {

template <class shape_storage_type>
class ExplicitOperators;

template <class shape_storage_type>
class ExplicitVectorOperators;

template <class shape_storage_type, class matrix_type, class rhs_type>
class ImplicitOperators;

template <class shape_storage_type, class matrix_type, class rhs_type>
class ImplicitVectorOperators;

/**
 * Efficiently stores shape functions of uniform length.
 * This class is used to store shape functions (stencil weights)
 * generated for discretizations where supports of all nodes have equal size
 * e.g. the support consists of 9 closest nodes. This class
 * is more efficient than storing the shapes in a nested type, such as
 * `std::vector<std::vector<T>>`, see [technical report](http://TODO(jureslak):).
 *
 * If supports of node vary in size, use RaggedShapeStorage instead.
 *
 * @tparam vec_t Vector type used in computations, specifies the dimensionality of the domain
 * and scalar type for numerical computations.
 * @tparam mask A bitmask indicating operators for which the shapes will be stored.
 * The masks for basic operators are located in namespace @ref sh.
 * These are sh::d1, sh::d2 and sh::lap. If you want to solve a Laplace equation with
 * Neumann BC, you would create a class with the shape `sh::d1 | sh::lap`.
 * The default is sh::all which prepares all shapes for first and second derivatives.
 *
 * if you try to call a function that works with other shapes than the ones allowed by
 * @ref mask, you will get a *compile time* error like:
 * @code
 * error: static_assert failed "D2 shapes must be initialized to use this function. Check your shape bitmask."
 *   static_assert(mask & sh::d2, "D2 shapes must be initialized to use this "
 *   ^             ~~~~~~~~~~~~~
 * @endcode
 *
 * Usage example:
 * @snippet UniformShapeStorage_test.cpp Uniform shape storage usage example
 *
 * @sa sh, RaggedShapeStorage, computeShapes
 */
template <typename vec_t, sh::shape_flags shape_mask = sh::all>
class UniformShapeStorage {
  public:
    static_assert(shape_mask != 0, "Cannot create an operator with no shapes.");
    typedef vec_t vector_t;  ///< Vector type used.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar type used.
    /// Bitmask telling us which shapes to create.
    enum { /** Mask value. */ mask = shape_mask };
    /// Store dimension of the domain.
    enum { /** Dimensionality of the domain. */ dim = vec_t::dim };
  private:
    /// Support size.
    int support_size_;
    /// Domain size.
    int domain_size_;
    /// Shape container for laplace operator.
    Range<scalar_t> shape_laplace_;
    /// Local copy of support domains.
    Range<int> support_domain;
    /// Shape container for first derivatives.
    Range<scalar_t> shape_d1_;
    /** shape container for second derivatives. Access shape for
     *  @f[
     *    \left. \frac{\partial^2}{\partial x_i \partial x_j}\right|_n \ ,
     *    \ \ \ \ i \leq j
     *  @f]
     *  derivation by starting take the following elements
     *  @f[
     *    \mathrm{support\_size}\,\left(
     *      \frac{1}{2}\mathrm{dim}\, (\mathrm{dim}+1)\,\mathrm{node} +
     *      \frac{1}{2} j \, (j+1) + i
     *      \right) + k \ ,
     *    \ \ \ \ k=0,1, \dots, \mathrm{support\_size}-1
     *  @f]
     */
    Range<scalar_t> shape_d2_;
  public:
    /// Constructs an empty shape storage with @ref size 0.
    UniformShapeStorage() : support_size_(0), domain_size_(0) {}

    /**
     * Resizes the storage to accommodate shapes of given sizes. If support sizes are `{9, 9, 9}`
     * the class will allocate space for shapes for 3 nodes with 9 support nodes each.
     * The containers are zero initialized.
     * @throws Assertion fails if the elements of `support_sizes` are not all the same.
     */
    void resize(const Range<int>& support_sizes);

    /// Returns number of nodes.
    int size() const { return domain_size_; }

    /**
     * Returns index of `j`-th neighbour of `node`-th node, ie. `support[node][j]`,
     * but possibly faster.
     */
    SINL int support(int node, int j) const { return support_domain[node * support_size_ + j]; }

    /// Sets support of `node`-th node to `support`.
    SINL void setSupport(int node, const Range<int>& support);

    /// Returns support size of `node`-th node.
    int supportSize(int /* node */) const { return support_size_; }

    /// Returns a vector of support sizes for all nodes, useful for matrix space prealocation.
    Range<int> supportSizes() const { return Range<int>(domain_size_, support_size_); }

    /// Return `j-th` laplace shape coefficient for `node`-th node.
    SINL scalar_t laplace(int node, int j) const;
    /// Returns the laplace shape for `node`.
    Eigen::Map<const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>> laplace(int node) const;
    /// Sets the laplace shape for `node` to `shape`.
    SINL void setLaplace(int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape);

    /// Return `j`-th shape coefficient for derivative wrt. variable `var` in `node`.
    SINL scalar_t d1(int var, int node, int j) const;
    /// Return shape for derivative wrt. variable `var` in `node`.
    Eigen::Map<const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>> d1(int var, int node) const;
    /// Sets shape for derivative wrt. variable `var` for `node` to `shape`.
    SINL void setD1(int var, int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape);

    /**
     * Return `j`-th shape coefficient for mixed derivative wrt. variables `varmin` and `varmax` in
     * `node`.
     */
    SINL scalar_t d2(int varmin, int varmax, int node, int j) const;
    /// Return shape for mixed derivative wrt. variables `varmin` and `varmax` in `node`.
    Eigen::Map<const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>>
    d2(int varmin, int varmax, int node) const;
    /// Sets shape for mixed derivative wrt. variables `varmin` and `varmax` for `node` to `shape`.
    SINL void setD2(int varmin, int varmax, int node,
                    const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape);

    /// Returns the approximate memory used (in bytes).
    size_t memoryUsed() const;

    /// Construct explicit operators over this storage.
    ExplicitOperators<UniformShapeStorage> explicitOperators() const;
    /// Construct explicit vector operators over this storage.
    ExplicitVectorOperators<UniformShapeStorage> explicitVectorOperators() const;
    /// Construct implicit operators over this storage.
    template <typename M, typename R>
    ImplicitOperators<UniformShapeStorage, M, R> implicitOperators(M& matrix, R& rhs) const;
    /// Construct implicit vector operators over this storage.
    template <typename M, typename R>
    ImplicitVectorOperators<UniformShapeStorage, M, R> implicitVectorOperators(
            M& matrix, R& rhs) const;

    /// Output basic info about this shape storage.
    template <typename V, sh::shape_flags M>
    friend std::ostream& operator<<(std::ostream& os, const UniformShapeStorage<V, M>& shapes);
};

}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_UNIFORMSHAPESTORAGE_FWD_HPP_
