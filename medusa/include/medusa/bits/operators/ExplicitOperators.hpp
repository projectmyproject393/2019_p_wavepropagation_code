#ifndef MEDUSA_BITS_OPERATORS_EXPLICITOPERATORS_HPP_
#define MEDUSA_BITS_OPERATORS_EXPLICITOPERATORS_HPP_

/**
 * @file
 * Explicit operators implementation.
 */

#include "ExplicitOperators_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>

namespace mm {

/**
 * Performs all asserts for a given node: asserts that node index is valid and that shape functions
 * were initialized for a given node.
 * @param node Node index to consider.
 */
#define NODE_ASSERTS(node) \
    assert_msg(0 <= (node) && (node) < ss->size(), "Node index %d must be in range " \
               "[0, num_nodes = %d).", ss->size());

/// @cond
template <class shape_storage_type>
template <class scalar_field_t>
typename ExplicitOperators<shape_storage_type>::scalar_t
ExplicitOperators<shape_storage_type>::lap(const scalar_field_t& u, int node) const {
    NODE_ASSERTS(node);
    scalar_t lap = 0;
    for (int i = 0; i < ss->supportSize(node); ++i) {
        lap += ss->laplace(node, i) * u[ss->support(node, i)];
    }
    return lap;
}

template <class shape_storage_type>
template <class scalar_field_t>
typename ExplicitOperators<shape_storage_type>::vector_t
ExplicitOperators<shape_storage_type>::grad(const scalar_field_t& u, int node) const {
    NODE_ASSERTS(node);
    vector_t ret = static_cast<scalar_t>(0.);
    for (int var = 0; var < dim; ++var)  // loop over dimensions
        for (int i = 0; i < ss->supportSize(node); ++i)
            ret[var] += ss->d1(var, node, i) * u[ss->support(node, i)];
    return ret;
}

template <class shape_storage_type>
template <class scalar_field_t>
typename ExplicitOperators<shape_storage_type>::scalar_t
ExplicitOperators<shape_storage_type>::d1(const scalar_field_t& u, int var, int node) {
    NODE_ASSERTS(node);
    scalar_t ret = 0.0;
    for (int i = 0; i < ss->supportSize(node); ++i)
        ret += ss->d1(var, node, i) * u[ss->support(node, i)];
    return ret;
}

template <class shape_storage_type>
template <class scalar_field_t>
typename ExplicitOperators<shape_storage_type>::scalar_t
ExplicitOperators<shape_storage_type>::d2(const scalar_field_t& u, int varmin, int varmax,
                                          int node) {
    NODE_ASSERTS(node);
    scalar_t ret = 0.0;
    for (int i = 0; i < ss->supportSize(node); ++i)
        ret += ss->d2(varmin, varmax, node, i) * u[ss->support(node, i)];
    return ret;
}

template <class shape_storage_type>
template <class vector_field_t>
typename ExplicitOperators<shape_storage_type>::scalar_t
ExplicitOperators<shape_storage_type>::neumann(const vector_field_t& u, int node,
                                               const vector_t& normal,
                                               scalar_t val) const {
    NODE_ASSERTS(node);
    assert_msg(ss->support(node, 0) == node, "First support node should be the node itself.");
    scalar_t denominator = 0;
    for (int d = 0; d < dim; ++d) {
        for (int i = 1; i < ss->supportSize(node); ++i) {
            val -= normal[d] * ss->d1(d, node, i) * u[ss->support(node, i)];
        }
        // i = 0
        denominator += normal[d] * ss->d1(d, node, 0);
    }
    assert_msg(std::abs(denominator) >= 1e-14,
               "Node %d has no effect on the flux in direction %s. The cause of this might be wrong"
               " normal direction, bad neighbourhood choice or bad nodal positions.", node, normal);
    return val / denominator;
}
/// @endcond

/// Output basic information about given operators.
template <typename S>
std::ostream& operator<<(std::ostream& os, const ExplicitOperators<S>& op)  {
    if (!op.hasShapes()) {
        return os << "Explicit operators without any linked storage.";
    }
    return os << "Explicit operators over storage: " << *op.ss;
}

template <typename vec_t, sh::shape_flags shape_mask>
ExplicitOperators<UniformShapeStorage<vec_t, shape_mask>>
UniformShapeStorage<vec_t, shape_mask>::explicitOperators() const {
    return ExplicitOperators<UniformShapeStorage<vec_t, shape_mask>>(*this);
}

template <typename vec_t, sh::shape_flags shape_mask>
ExplicitOperators<RaggedShapeStorage<vec_t, shape_mask>>
RaggedShapeStorage<vec_t, shape_mask>::explicitOperators() const {
    return ExplicitOperators<RaggedShapeStorage<vec_t, shape_mask>>(*this);
}

}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_EXPLICITOPERATORS_HPP_
