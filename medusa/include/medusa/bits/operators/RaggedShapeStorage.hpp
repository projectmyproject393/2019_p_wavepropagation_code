#ifndef MEDUSA_BITS_OPERATORS_RAGGEDSHAPESTORAGE_HPP_
#define MEDUSA_BITS_OPERATORS_RAGGEDSHAPESTORAGE_HPP_

/**
 * @file
 * Implementation of shape storage for shapes of different sizes.
 */

#include "RaggedShapeStorage_fwd.hpp"
#include "printShapes.hpp"
#include "shape_flags.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <cstring>
#include <Eigen/Core>

namespace mm {

template <typename vec_t, sh::shape_flags shape_mask>
void RaggedShapeStorage<vec_t, shape_mask>::resize(const Range<int>& support_sizes) {
    domain_size_ = support_sizes.size();
    support_sizes_ = support_sizes;

    support_starts_.resize(domain_size_, 0);
    for (int i = 1; i < domain_size_; ++i) {
        support_starts_[i] = support_starts_[i-1] + support_sizes_[i-1];
    }
    total_size_ = support_starts_[domain_size_-1] + support_sizes_[domain_size_-1];

    // Fills support with -1, to indicate which nodes have their shapes computed.
    support_domain.resize(total_size_, -1);

    // checks flags
    if (shape_mask & sh::d1) {
        shape_d1_.resize(dim * total_size_, 0);
    }
    if (shape_mask & sh::lap) {
        shape_laplace_.resize(total_size_, 0);
    }
    if (shape_mask & sh::d2) {
        shape_d2_.resize(dim * (dim + 1) * total_size_ / 2, 0);
    }
}

template <typename vec_t, sh::shape_flags shape_mask>
typename RaggedShapeStorage<vec_t, shape_mask>::scalar_t
RaggedShapeStorage<vec_t, shape_mask>::laplace(int node, int j) const {
    static_assert(mask & sh::lap, "Laplace shape must be initialized to use this function. "
                                  "Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= j && j < support_sizes_[node], "Support index %d out of range [0, %d).",
               j, support_sizes_[node]);
    return shape_laplace_[support_starts_[node] + j];
}

template <typename vec_t, sh::shape_flags shape_mask>
typename RaggedShapeStorage<vec_t, shape_mask>::scalar_t
RaggedShapeStorage<vec_t, shape_mask>::d1(int var, int node, int j) const {
    static_assert(mask & sh::d1, "D1 shapes must be initialized to use this function. "
                                 "Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= j && j < support_sizes_[node], "Support index %d out of range [0, %d).",
               j, support_sizes_[node]);
    assert_msg(0 <= var && var < dim, "Variable index %d out of bounds [%d, %d).", var, 0, dim);
    return shape_d1_[dim * support_starts_[node] + var * support_sizes_[node] + j];
}

template <typename vec_t, sh::shape_flags shape_mask>
typename RaggedShapeStorage<vec_t, shape_mask>::scalar_t
RaggedShapeStorage<vec_t, shape_mask>::d2(
        int varmin, int varmax, int node, int j) const {
    static_assert(mask & sh::d2, "D2 shapes must be initialized to use this "
                                 "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= j && j < support_sizes_[node], "Support index %d out of range [0, %d).",
               j, support_sizes_[node]);
    assert_msg(0 <= varmin && varmin < dim,
               "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
    assert_msg(0 <= varmax && varmax < dim,
               "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
    assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
               varmin, varmax);
    int node_idx = dim * (dim + 1)/ 2 * support_starts_[node];
    int dim_idx = (varmax * (varmax + 1) / 2 + varmin);
    return shape_d2_[node_idx + dim_idx * support_sizes_[node] + j];
}

template <typename vec_t, sh::shape_flags shape_mask>
Eigen::Map<const Eigen::Matrix<typename RaggedShapeStorage<vec_t, shape_mask>::scalar_t,
        Eigen::Dynamic, 1>>
RaggedShapeStorage<vec_t, shape_mask>::laplace(int node) const {
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    static_assert(mask & sh::lap, "Laplace shape must be initialized to use this "
                                  "function. Check your shape bitmask.");
    return {shape_laplace_.data() + support_starts_[node], support_sizes_[node]};
}

template <typename vec_t, sh::shape_flags shape_mask>
Eigen::Map<const Eigen::Matrix<typename RaggedShapeStorage<vec_t, shape_mask>::scalar_t,
        Eigen::Dynamic, 1>>
RaggedShapeStorage<vec_t, shape_mask>::d1(int var, int node) const {
    static_assert(mask & sh::d1, "D1 shapes must be initialized to use this function."
                                 " Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= var && var < dim, "Variable index %d out of bounds [%d, %d).", var, 0, dim);
    return {shape_d1_.data() + dim * support_starts_[node] + var * support_sizes_[node],
            support_sizes_[node]};
}

template <typename vec_t, sh::shape_flags shape_mask>
Eigen::Map<const Eigen::Matrix<typename RaggedShapeStorage<vec_t, shape_mask>::scalar_t,
        Eigen::Dynamic, 1>>
RaggedShapeStorage<vec_t, shape_mask>::d2(int varmin, int varmax, int node) const {
    static_assert(mask & sh::d2, "D2 shapes must be initialized to use this "
                                 "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= varmin && varmin < dim,
               "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
    assert_msg(0 <= varmax && varmax < dim,
               "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
    assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
               varmin, varmax);
    int node_idx = dim * (dim + 1)/ 2 * support_starts_[node];
    int dim_idx = (varmax * (varmax + 1) / 2 + varmin);
    return {shape_d2_.data() + node_idx + dim_idx * support_sizes_[node], support_sizes_[node]};
}

template <typename vec_t, sh::shape_flags shape_mask>
void RaggedShapeStorage<vec_t, shape_mask>::setSupport(
        int node, const Range<int>& support) {
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(support.size() == support_sizes_[node],
               "Support of unexpected length %d, expected %d.",
               support.size(), support_sizes_[node]);
    std::memcpy(support_domain.data() + support_starts_[node], support.data(),
                support.size()*sizeof(int));
}

template <typename vec_t, sh::shape_flags shape_mask>
void RaggedShapeStorage<vec_t, shape_mask>::setLaplace(
        int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape) {
    static_assert(mask & sh::lap, "Laplace shape must be initialized to use this "
                                  "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(shape.size() == support_sizes_[node],
               "Laplace shape of unexpected length %d, expected %d.",
               shape.size(), support_sizes_[node]);
    std::memcpy(shape_laplace_.data() + support_starts_[node], shape.data(),
                support_sizes_[node]*sizeof(scalar_t));
}

template <typename vec_t, sh::shape_flags shape_mask>
void RaggedShapeStorage<vec_t, shape_mask>::setD1(
        int var, int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape) {
    static_assert(mask & sh::d1, "D1 shapes must be initialized to use this function."
                                 " Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= var && var < dim, "Variable index %d out of bounds [%d, %d).", var, 0, dim);
    assert_msg(shape.size() == support_sizes_[node], "D1 shape of unexpected length %d, expected "
                                                     "%d.", shape.size(), support_sizes_[node]);
    std::memcpy(shape_d1_.data() + dim * support_starts_[node] + var * support_sizes_[node],
                shape.data(), support_sizes_[node]*sizeof(scalar_t));
}

template <typename vec_t, sh::shape_flags shape_mask>
void RaggedShapeStorage<vec_t, shape_mask>::setD2(
        int varmin, int varmax, int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape) {
    static_assert(mask & sh::d2, "D2 shapes must be initialized to use this "
                                 "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= varmin && varmin < dim,
               "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
    assert_msg(0 <= varmax && varmax < dim,
               "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
    assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
               varmin, varmax);
    assert_msg(shape.size() == support_sizes_[node], "D2 shape of unexpected length %d, expected "
                                                     "%d.", shape.size(), support_sizes_[node]);
    int node_idx = dim * (dim + 1)/ 2 * support_starts_[node];
    int dim_idx = (varmax * (varmax + 1) / 2 + varmin);
    std::memcpy(shape_d2_.data() + node_idx + dim_idx * support_sizes_[node], shape.data(),
                support_sizes_[node]*sizeof(scalar_t));
}

/// Output basic info about this shape storage.
template <typename V, sh::shape_flags M>
std::ostream& operator<<(std::ostream& os, const RaggedShapeStorage<V, M>& shapes) {
    os << "Ragged shape storage:\n";
    return shapes_internal::printShapes(shapes, os);
}

template <typename vec_t, sh::shape_flags shape_mask>
size_t RaggedShapeStorage<vec_t, shape_mask>::memoryUsed() const {
    return sizeof(*this) + mem_used(support_domain) + mem_used(shape_laplace_) +
           mem_used(shape_d1_) + mem_used(shape_d2_) + mem_used(support_sizes_) +
           mem_used(support_starts_);
}

}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_RAGGEDSHAPESTORAGE_HPP_
