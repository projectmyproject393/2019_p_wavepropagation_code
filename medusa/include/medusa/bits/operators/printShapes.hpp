#ifndef MEDUSA_BITS_OPERATORS_PRINTSHAPES_HPP_
#define MEDUSA_BITS_OPERATORS_PRINTSHAPES_HPP_

/**
 * @file
 * Suppport functions for shape printing.
 */

#include "shape_flags.hpp"
#include <iostream>
#include <medusa/bits/utils/memutils.hpp>

namespace mm {
namespace shapes_internal {

/// @cond
template <typename shape_storage_t>
typename std::enable_if<!(shape_storage_t::mask & sh::lap)>::type
printLapShapes(const shape_storage_t&, std::ostream&) {}

template <typename shape_storage_t>
typename std::enable_if<!!(shape_storage_t::mask & sh::lap)>::type
printLapShapes(const shape_storage_t& storage, std::ostream& os) {
    int N = std::min(storage.size(), 5);
    os << "    Lap shapes sample:\n";
    for (int i = 0; i < N; ++i) {
        os << "        node " << i << ":";
        for (int j = 0; j < storage.supportSize(i); ++j) {
            os << " " << storage.laplace(i, j);
        }
        os << '\n';
    }
}

template <typename shape_storage_t>
typename std::enable_if<!(shape_storage_t::mask & sh::d1)>::type
printD1Shapes(const shape_storage_t&, std::ostream&) {}

template <typename shape_storage_t>
typename std::enable_if<!!(shape_storage_t::mask & sh::d1)>::type
printD1Shapes(const shape_storage_t& storage, std::ostream& os) {
    os << "    D1 shapes sample:\n";
    int N = std::min(storage.size(), 5);
    for (int i = 0; i < N; ++i) {
        os << "        node " << i << ":\n";
        for (int d = 0; d < storage.dim; ++d) {
            os << "          " << "dim " << d << ":";
            for (int j = 0; j < storage.supportSize(i); ++j) {
                os << " " << storage.d1(d, i, j);
            }
            os << '\n';
        }
    }
}

template <typename shape_storage_t>
typename std::enable_if<!(shape_storage_t::mask & sh::d2)>::type
printD2Shapes(const shape_storage_t&, std::ostream&) {}

template <typename shape_storage_t>
typename std::enable_if<!!(shape_storage_t::mask & sh::d2)>::type
printD2Shapes(const shape_storage_t& storage, std::ostream& os) {
    os << "    D2 shapes sample:\n";
    int N = std::min(storage.size(), 5);
    for (int i = 0; i < N; ++i) {
        os << "        node " << i << ":\n";
        for (int d = 0; d < storage.dim; ++d) {
            for (int d2 = 0; d2 <= d; ++d2) {
                os << "          " << "dims " << d2 << " " << d << ":";
                for (int j = 0; j < storage.supportSize(i); ++j) {
                    os << " " << storage.d2(d2, d, i, j);
                }
                os << '\n';
            }
        }
    }
}
/// @endcond

/// Output basic info abour given shape storage to `os`.
template <typename shape_storage_t>
std::ostream& printShapes(const shape_storage_t& storage, std::ostream& os) {
    os << "    dimension: " << storage.dim << '\n'
       << "    domain size: " << storage.size() << '\n'
       << "    mask = " << sh::str(storage.mask) << '\n';

    printLapShapes(storage, os);
    printD1Shapes(storage, os);
    printD2Shapes(storage, os);

    std::size_t memory_used = storage.memoryUsed();
    os << "    mem used total: " << mem2str(memory_used);
    return os;
}

}  // namespace shapes_internal
}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_PRINTSHAPES_HPP_
