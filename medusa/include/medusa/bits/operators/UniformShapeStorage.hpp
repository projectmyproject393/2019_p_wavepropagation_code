#ifndef MEDUSA_BITS_OPERATORS_UNIFORMSHAPESTORAGE_HPP_
#define MEDUSA_BITS_OPERATORS_UNIFORMSHAPESTORAGE_HPP_

/**
 * @file
 * Implementation of shape storage for uniformly long shapes.
 */

#include "UniformShapeStorage_fwd.hpp"
#include "printShapes.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <cstring>
#include <Eigen/Core>

namespace mm {

template <typename vec_t, sh::shape_flags shape_mask>
void UniformShapeStorage<vec_t, shape_mask>::resize(const Range<int>& support_sizes) {
    domain_size_ = support_sizes.size();
    support_size_ = (domain_size_ > 0) ? support_sizes[0] : 0;
    for (int s : support_sizes) {
        assert_msg(support_size_ == s, "Not all support sizes are equal, got sizes %d and %d. "
                                       "Use RaggedShapeStorage instead.", support_size_, s);
    }

    // Fills support with -1, to indicate which nodes have their shapes computed.
    support_domain.resize(domain_size_ * support_size_, -1);

    // checks flags
    if (shape_mask & sh::d1) {
        shape_d1_.resize(domain_size_ * dim * support_size_, 0);
    }
    if (shape_mask & sh::lap) {
        shape_laplace_.resize(domain_size_ * support_size_, 0);
    }
    if (shape_mask & sh::d2) {
        shape_d2_.resize(domain_size_ * dim * (dim + 1) * support_size_ / 2, 0);
    }
}

template <typename vec_t, sh::shape_flags shape_mask>
typename UniformShapeStorage<vec_t, shape_mask>::scalar_t
UniformShapeStorage<vec_t, shape_mask>::laplace(int node, int j) const {
    static_assert(mask & sh::lap, "Laplace shape must be initialized to use this function. "
                                  "Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= j && j < support_size_, "Support index %d out of range [0, %d).",
               j, support_size_);
    return shape_laplace_[node * support_size_ + j];
}

template <typename vec_t, sh::shape_flags shape_mask>
typename UniformShapeStorage<vec_t, shape_mask>::scalar_t
UniformShapeStorage<vec_t, shape_mask>::d1(int var, int node, int j) const {
    static_assert(mask & sh::d1, "D1 shapes must be initialized to use this function. "
                                 "Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= j && j < support_size_, "Support index %d out of range [0, %d).",
               j, support_size_);
    assert_msg(0 <= var && var < dim, "Variable index %d out of bounds [%d, %d).", var, 0, dim);
    return shape_d1_[dim * support_size_ * node + var * support_size_ + j];
}

template <typename vec_t, sh::shape_flags shape_mask>
typename UniformShapeStorage<vec_t, shape_mask>::scalar_t
UniformShapeStorage<vec_t, shape_mask>::d2(
        int varmin, int varmax, int node, int j) const {
    static_assert(mask & sh::d2, "D2 shapes must be initialized to use this "
            "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= j && j < support_size_, "Support index %d out of range [0, %d).",
               j, support_size_);
    assert_msg(0 <= varmin && varmin < dim,
               "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
    assert_msg(0 <= varmax && varmax < dim,
               "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
    assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
               varmin, varmax);
    int idx = dim * (dim+1)/2 * node + (varmax*(varmax+1)/2 + varmin);
    return shape_d2_[idx * support_size_ + j];
}

template <typename vec_t, sh::shape_flags shape_mask>
Eigen::Map<const Eigen::Matrix<typename UniformShapeStorage<vec_t, shape_mask>::scalar_t,
                               Eigen::Dynamic, 1>>
UniformShapeStorage<vec_t, shape_mask>::laplace(int node) const {
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    static_assert(mask & sh::lap, "Laplace shape must be initialized to use this "
                                  "function. Check your shape bitmask.");
    return {shape_laplace_.data() + (node*support_size_), support_size_};
}

template <typename vec_t, sh::shape_flags shape_mask>
Eigen::Map<const Eigen::Matrix<typename UniformShapeStorage<vec_t, shape_mask>::scalar_t,
                               Eigen::Dynamic, 1>>
UniformShapeStorage<vec_t, shape_mask>::d1(int var, int node) const {
    static_assert(mask & sh::d1, "D1 shapes must be initialized to use this function."
                                 " Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= var && var < dim, "Variable index %d out of bounds [%d, %d).", var, 0, dim);
    return {shape_d1_.data() + dim * support_size_ * node + var * support_size_, support_size_};
}

template <typename vec_t, sh::shape_flags shape_mask>
Eigen::Map<const Eigen::Matrix<typename UniformShapeStorage<vec_t, shape_mask>::scalar_t,
                               Eigen::Dynamic, 1>>
UniformShapeStorage<vec_t, shape_mask>::d2(int varmin, int varmax, int node) const {
    static_assert(mask & sh::d2, "D2 shapes must be initialized to use this "
                                 "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= varmin && varmin < dim,
               "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
    assert_msg(0 <= varmax && varmax < dim,
               "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
    assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
               varmin, varmax);
    int idx = dim * (dim+1)/2 * node + (varmax*(varmax+1)/2 + varmin);
    return {shape_d2_.data() + idx*support_size_, support_size_};
}

template <typename vec_t, sh::shape_flags shape_mask>
void UniformShapeStorage<vec_t, shape_mask>::setSupport(
        int node, const Range<int>& support) {
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(support.size() == support_size_, "Support of unexpected length %d, expected %d.",
               support.size(), support_size_);
    std::memcpy(support_domain.data() + node * support_size_, support.data(),
                support.size()*sizeof(int));
}

template <typename vec_t, sh::shape_flags shape_mask>
void UniformShapeStorage<vec_t, shape_mask>::setLaplace(
        int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape) {
    static_assert(mask & sh::lap, "Laplace shape must be initialized to use this "
                                  "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(shape.size() == support_size_, "Laplace shape of unexpected length %d, expected %d.",
               shape.size(), support_size_);
    std::memcpy(shape_laplace_.data() + node * support_size_, shape.data(),
                support_size_*sizeof(scalar_t));
}

template <typename vec_t, sh::shape_flags shape_mask>
void UniformShapeStorage<vec_t, shape_mask>::setD1(
        int var, int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape) {
    static_assert(mask & sh::d1, "D1 shapes must be initialized to use this function."
                                 " Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= var && var < dim, "Variable index %d out of bounds [%d, %d).", var, 0, dim);
    assert_msg(shape.size() == support_size_, "D1 shape of unexpected length %d, expected %d.",
               shape.size(), support_size_);
    std::memcpy(shape_d1_.data() + dim * support_size_ * node + var * support_size_, shape.data(),
                support_size_*sizeof(scalar_t));
}

template <typename vec_t, sh::shape_flags shape_mask>
void UniformShapeStorage<vec_t, shape_mask>::setD2(
        int varmin, int varmax, int node, const Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>& shape) {
    static_assert(mask & sh::d2, "D2 shapes must be initialized to use this "
                                 "function. Check your shape bitmask.");
    assert_msg(0 <= node && node <= domain_size_, "Node index %d out or range [0, %d).",
               node, domain_size_);
    assert_msg(0 <= varmin && varmin < dim,
               "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
    assert_msg(0 <= varmax && varmax < dim,
               "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
    assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
               varmin, varmax);
    assert_msg(shape.size() == support_size_, "D2 shape of unexpected length %d, expected %d.",
               shape.size(), support_size_);
    int idx = dim * (dim+1)/2 * node + (varmax*(varmax+1)/2 + varmin);
    std::memcpy(shape_d2_.data() + idx*support_size_, shape.data(), support_size_*sizeof(scalar_t));
}

/// Output basic info about this shape storage.
template <typename V, sh::shape_flags M>
std::ostream& operator<<(std::ostream& os, const UniformShapeStorage<V, M>& shapes) {
    os << "Uniform shape storage:\n";
    return shapes_internal::printShapes(shapes, os);
}

template <typename vec_t, sh::shape_flags shape_mask>
size_t UniformShapeStorage<vec_t, shape_mask>::memoryUsed() const {
    return sizeof(*this) + mem_used(support_domain) + mem_used(shape_laplace_) +
           mem_used(shape_d1_) + mem_used(shape_d2_);
}

}  // namespace mm

#endif  // MEDUSA_BITS_OPERATORS_UNIFORMSHAPESTORAGE_HPP_
