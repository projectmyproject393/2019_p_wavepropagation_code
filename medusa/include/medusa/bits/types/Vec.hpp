#ifndef MEDUSA_BITS_TYPES_VEC_HPP_
#define MEDUSA_BITS_TYPES_VEC_HPP_

/**
 * @file
 * Implemetations for Vec.
 * @sa MatrixAddons.hpp
 * @sa MatrixBaseAddons.hpp
 */

#include "Vec_fwd.hpp"

namespace mm {

}  // namespace mm

#endif  // MEDUSA_BITS_TYPES_VEC_HPP_
