#ifndef MEDUSA_BITS_APPROXIMATIONS_WEIGHTFUNCTION_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_WEIGHTFUNCTION_HPP_

/**
 * @file
 * Implementations of weight functions.
 */

#include <cmath>
#include <medusa/bits/utils/assert.hpp>
#include "WeightFunction_fwd.hpp"

namespace mm {

template <class vec_t, class RBFType>
typename RBFWeight<vec_t, RBFType>::scalar_t RBFWeight<vec_t, RBFType>::operator()(
        const vector_t& point, const derivative_t& derivative) const {
    int totaldeg = 0;
    int maxdeg = 0;
    for (int x : derivative) {
        assert_msg(x >= 0, "Derivative of negative order %d requested.", x);
        totaldeg += x;
        if (x > maxdeg) {
            maxdeg = x;
        }
    }
    assert_msg(maxdeg <= 2, "Derivatives of order >= 3 not supported, got %s.", derivative);
    assert_msg(maxdeg != 2 || totaldeg == 2, "Mixed derivatives including second order terms "
                                             "are not supported, got %s.", derivative);
    scalar_t r2 = point.squaredNorm();
    scalar_t v = rbf_(r2, maxdeg);
    for (int d = 0; d < dim; ++d) {
        if (derivative[d] == 1) {
            v *= 2*point[d];
        } else if (derivative[d] == 2) {
            v *= 4*point[d]*point[d];
            v += 2*rbf_(r2, 1);
        }
    }
    return v;
}

/// Output info about given weight function.
template <typename V, typename R>
std::ostream& operator<<(std::ostream& os, const RBFWeight<V, R>& w) {
    return os << "RBFWeight constructed from " << w.rbf_;
}

/// @cond
template <class vec_t>
typename NoWeight<vec_t>::scalar_t NoWeight<vec_t>::operator()(
        const vec_t& /* point */, const NoWeight<vec_t>::derivative_t& derivative) const {
    for (int i = 0; i < dim; ++i) {
        if (derivative[i] > 0) {
            return 0.0;
        }
    }
    return 1.0;
}
/// @endcond

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_WEIGHTFUNCTION_HPP_
