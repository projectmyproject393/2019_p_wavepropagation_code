#ifndef MEDUSA_BITS_APPROXIMATIONS_WLS_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_WLS_HPP_

/**
 * @file
 * Implementation of weighted least squares approximation.
 */

#include "WLS_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/types/Range.hpp>
#include <medusa/bits/utils/numutils.hpp>

namespace mm {

template <class basis_t, class weight_t, class scale_t, class solver_t>
void WLS<basis_t, weight_t, scale_t, solver_t>::compute(const vector_t& point,
                                                        const std::vector<vector_t>& support) {
    int n = support.size();
    assert_msg(basis_.size() <= n,
               "WLS cannot have more basis functions than points in support domain, but %d "
               "support points and %d basis functions were given.", n, basis_.size());
    scale_ = scale_t::scale(point, support);
    local_coordinates.resize(n);
    for (int i = 0; i < n; ++i) {
        local_coordinates[i] = (support[i] - point) / scale_;
    }

    // Evaluate basis functions in given points
    int m = basis_.size();
    ei_matrix_t WB(m, n);
    for (int j = 0; j < n; j++) {
        scalar_t w = weight_(local_coordinates[j]);
        for (int i = 0; i < m; i++) {
            WB(i, j) = w * basis_(i, local_coordinates[j], local_coordinates);
        }
    }
    solver_.compute(WB);
}

template <class basis_t, class weight_t, class scale_t, class solver_t>
Eigen::Matrix<typename basis_t::scalar_t, Eigen::Dynamic, 1>
WLS<basis_t, weight_t, scale_t, solver_t>::getShape() const {
    int m = basis_.size();
    int n = local_coordinates.size();

    // Step 1: get Lb aka. the right hand side
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> b(m);
    for (int i = 0; i < m; i++) {
        b(i) = basis_.evalAt0(i, local_coordinates);
    }

    // Step 2: solve (WB)^T x = L.b
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> result = solver_.solve(b);

    // Step 3: multiply with weight once again
    for (int j = 0; j < n; j++) {
        result(j) *= weight_(local_coordinates[j]);
    }

    return result;
}

template <class basis_t, class weight_t, class scale_t, class solver_t>
Eigen::Matrix<typename basis_t::scalar_t, Eigen::Dynamic, 1>
WLS<basis_t, weight_t, scale_t, solver_t>::getShape(const derivative_t& der) const {
    int m = basis_.size();
    int n = local_coordinates.size();

    // Step 1: get Lb aka. the right hand side
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> Lb(m);
    for (int i = 0; i < m; i++) {
        Lb(i) = basis_.evalAt0(i, der, local_coordinates);
    }

    // Step 2: solve (WB)^T x = L.b
    Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> result = solver_.solve(Lb);

    // Step 3: multiply with weight and scale once again
    for (int j = 0; j < n; j++) {
        result(j) *= weight_(local_coordinates[j]);
    }

    int der_order = 0;
    for (int i = 0; i < dim; ++i) { der_order += der[i]; }
    return result / ipow(scale_, der_order);
}

template <class basis_t, class weight_t, class scale_t, class solver_t>
const basis_t& WLS<basis_t, weight_t, scale_t, solver_t>::basis() const {
    return basis_;
}

template <class basis_t, class weight_t, class scale_t, class solver_t>
const weight_t& WLS<basis_t, weight_t, scale_t, solver_t>::weight() const {
    return weight_;
}

template <class basis_t, class weight_t, class scale_t, class solver_t>
const solver_t& WLS<basis_t, weight_t, scale_t, solver_t>::solver() const {
    return solver_;
}

template <class basis_t, class weight_t, class scale_t, class solver_t>
solver_t& WLS<basis_t, weight_t, scale_t, solver_t>::solver() {
    return solver_;
}

/// Output basic info about given WLS class.
template <class basis_t, class weight_t, class scale_t, class solver_t>
std::ostream& operator<<(std::ostream& os, const WLS<basis_t, weight_t, scale_t, solver_t>& wls) {
    os << "WLS:\n"
       << "    dimension: " << wls.dim << '\n'
       << "    basis size: " << wls.basis_.size() << '\n'
       << "    basis: "  << wls.basis_ << '\n'
       << "    weight: "  << wls.weight_ << '\n'
       << "    scale: "  << scale_t();
    return os;
}

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_WLS_HPP_
