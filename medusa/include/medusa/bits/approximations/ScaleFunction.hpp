#ifndef MEDUSA_BITS_APPROXIMATIONS_SCALEFUNCTION_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_SCALEFUNCTION_HPP_

/**
 * @file
 * Scale function definition and implementation.
 */

#include <medusa/Config.hpp>
#include <medusa/bits/types/Range.hpp>
#include <iosfwd>

namespace mm {

/// Scale function that scales to the nearest neighbor.
class ScaleToClosest {
  public:
    /// Returns local scale of given point and its support.
    template<typename vec_t>
    static inline typename vec_t::scalar_t scale(const vec_t& p,
                                                 const std::vector<vec_t>& support) {
        return (p - support[1]).norm();
    }
    /// Print information about this scale function.
    inline friend std::ostream& operator<<(std::ostream& os, const ScaleToClosest& /* scale */) {
        return os << "ScaleToClosest";
    }
};

/// Scale function that scales to the farthest neighbor.
class ScaleToFarthest {
  public:
    /// Returns local scale of given point and its support.
    template<typename vec_t>
    static inline typename vec_t::scalar_t scale(const vec_t& p,
                                                 const std::vector<vec_t>& support) {
        return (p - support.back()).norm();
    }
    /// Print information about this scale function.
    inline friend std::ostream& operator<<(std::ostream& os, const ScaleToFarthest& /* scale */) {
        return os << "ScaleToFarthest";
    }
};

/// Scale function that does not scale.
class NoScale {
  public:
    template<typename vec_t>
    /// Returns local scale of given point and its support.
    static inline typename vec_t::scalar_t scale(
            const vec_t& /* p */, const std::vector<vec_t>& /* support */) { return 1.0; }
    /// Print information about this scale function.
    inline friend std::ostream& operator<<(std::ostream& os, const NoScale& /* scale */) {
        return os << "NoScale";
    }
};

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_SCALEFUNCTION_HPP_
