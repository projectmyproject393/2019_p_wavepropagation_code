#ifndef MEDUSA_BITS_APPROXIMATIONS_RBFBASIS_HPP_
#define MEDUSA_BITS_APPROXIMATIONS_RBFBASIS_HPP_

/**
 * @file
 * Implementation of RBF basis.
 */

#include <medusa/bits/utils/assert.hpp>
#include "RBFBasis_fwd.hpp"

namespace mm {

/// @cond
template <class vec_t, class RBFType>
typename RBFBasis<vec_t, RBFType>::scalar_t RBFBasis<vec_t, RBFType>::operator()(
        int index, const vector_t& point, const derivative_t& derivative,
        const std::vector<vector_t>& support) const {
    assert_msg(0 <= index && index < size_, "Basis index %d out of range [0, %d)", index, size_);
    assert_msg(static_cast<int>(support.size()) >= size_,
               "Not enough support points given for this basis size, got %d support points for "
               "basis size %d.", support.size(), size_);
    int totaldeg = 0;
    int maxdeg = 0;
    for (int x : derivative) {
        assert_msg(x >= 0, "Derivative of negative order %d requested.", x);
        totaldeg += x;
        if (x > maxdeg) {
            maxdeg = x;
        }
    }
    assert_msg(maxdeg <= 2, "Derivatives of order >= 3 not supported, got %s.", derivative);
    assert_msg(maxdeg != 2 || totaldeg == 2, "Mixed derivatives including second order terms "
                                             "are not supported, got %s.", derivative);
    auto& s = support[index];
    scalar_t r2 = (point-s).squaredNorm();
    scalar_t v = rbf_(r2, totaldeg);
    for (int d = 0; d < dim; ++d) {
        if (derivative[d] == 1) {
            v *= 2*(point[d] - s[d]);
        } else if (derivative[d] == 2) {
            v *= 4*(point[d] - s[d])*(point[d] - s[d]);
            v += 2*rbf_(r2, 1);
        }
    }
    return v;
}

template <class vec_t, class RBFType>
typename RBFBasis<vec_t, RBFType>::scalar_t RBFBasis<vec_t, RBFType>::operator()(
        int index, const vector_t& point, const std::vector<vector_t>& support) const {
    assert_msg(0 <= index && index < size_, "Basis index %d out of range [0, %d)", index, size_);
    assert_msg(static_cast<int>(support.size()) >= size_,
               "Not enough support points given for this basis size, got %d support points for "
               "basis size %d.", support.size(), size_);
    return rbf_((point-support[index]).squaredNorm());
}
/// @endcond

template <class vec_t, class RBFType>
typename RBFBasis<vec_t, RBFType>::scalar_t RBFBasis<vec_t, RBFType>::evalAt0(
        int index, const std::vector<vector_t>& support) const {
    assert_msg(0 <= index && index < size_, "Basis index %d out of range [0, %d)", index, size_);
    assert_msg(static_cast<int>(support.size()) >= size_,
               "Not enough support points given for this basis size, got %d support points for "
               "basis size %d.", support.size(), size_);
    return rbf_(support[index].squaredNorm());
}

template <class vec_t, class RBFType>
typename RBFBasis<vec_t, RBFType>::scalar_t RBFBasis<vec_t, RBFType>::evalAt0(
        int index, const derivative_t& derivative, const std::vector<vector_t>& support) const {
    assert_msg(0 <= index && index < size_, "Basis index %d out of range [0, %d)", index, size_);
    assert_msg(static_cast<int>(support.size()) >= size_,
               "Not enough support points given for this basis size, got %d support points for "
               "basis size %d.", support.size(), size_);
    int totaldeg = 0;
    int maxdeg = 0;
    for (int x : derivative) {
        assert_msg(x >= 0, "Derivative of negative order %d requested.", x);
        totaldeg += x;
        if (x > maxdeg) {
            maxdeg = x;
        }
    }
    assert_msg(maxdeg <= 2, "Derivatives of order >= 3 not supported, got %s.", derivative);
    assert_msg(maxdeg != 2 || totaldeg == 2, "Mixed derivatives including second order terms "
                                             "are not supported, got %s.", derivative);
    auto& s = support[index];
    scalar_t r2 = s.squaredNorm();
    scalar_t v = rbf_(r2, totaldeg);
    for (int d = 0; d < dim; ++d) {
        if (derivative[d] == 1) {
            v *= -2*s[d];
        } else if (derivative[d] == 2) {
            v *= 4*s[d]*s[d];
            v += 2*rbf_(r2, 1);
        }
    }
    return v;
}

/// Output basic info about given basis.
template <typename V, typename R>
std::ostream& operator<<(std::ostream& os, const RBFBasis<V, R>& m) {
    return os << "RBF basis " << m.dim << "D, " << m.size() << "x " << m.rbf();
}

}  // namespace mm

#endif  // MEDUSA_BITS_APPROXIMATIONS_RBFBASIS_HPP_
