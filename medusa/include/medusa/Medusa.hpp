#ifndef MEDUSA_MEDUSA_HPP_
#define MEDUSA_MEDUSA_HPP_

/**
 * @file
 * Main header file that includes most other components.
 */

#include "Config.hpp"
#include "Medusa_fwd.hpp"

#include "Domain.hpp"
#include "Approximations.hpp"
#include "Integrators.hpp"
#include "IO.hpp"
#include "Operators.hpp"
#include "Types.hpp"
#include "Utils.hpp"

#endif  // MEDUSA_MEDUSA_HPP_
