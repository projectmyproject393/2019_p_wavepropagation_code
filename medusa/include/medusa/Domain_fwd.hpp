#ifndef MEDUSA_DOMAIN_FWD_HPP_
#define MEDUSA_DOMAIN_FWD_HPP_

/**
 * @file
 * Header for Domain module, declarations only.
 */

#include "Config.hpp"

#include "bits/domains/BasicRelax_fwd.hpp"
#include "bits/domains/BoxShape_fwd.hpp"
#include "bits/domains/CircleShape_fwd.hpp"
#include "bits/domains/DomainDiscretization_fwd.hpp"
#include "bits/domains/DomainShape_fwd.hpp"
#include "bits/domains/FindBalancedSupport_fwd.hpp"
#include "bits/domains/FindClosest_fwd.hpp"
#include "bits/domains/PoissonDiskSampling_fwd.hpp"
#include "bits/domains/PolygonShape_fwd.hpp"
#include "bits/domains/ShapeDifference_fwd.hpp"
#include "bits/domains/ShapeUnion_fwd.hpp"
#include "bits/domains/UnknownShape_fwd.hpp"

#endif  // MEDUSA_DOMAIN_FWD_HPP_
