/**
 * @file
 * Instantiations of common RBFFD approximations.
 */

#include <medusa/bits/approximations/RBFFD.hpp>

#include <medusa/bits/approximations/Gaussian.hpp>
#include <medusa/bits/approximations/ScaleFunction.hpp>
#include <medusa/bits/approximations/Multiquadric.hpp>
#include <medusa/bits/approximations/InverseMultiquadric.hpp>
#include <medusa/bits/approximations/RBFBasis.hpp>
#include <medusa/bits/types/Vec.hpp>

#include <Eigen/Cholesky>

namespace mm {
template class RBFFD<Gaussian<double>, Vec2d, ScaleToFarthest>;
template class RBFFD<Multiquadric<double>, Vec2d, ScaleToFarthest>;
template class RBFFD<InverseMultiquadric<double>, Vec2d, ScaleToFarthest>;
}  // namespace mm
