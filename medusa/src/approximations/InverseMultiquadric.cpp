#include "medusa/bits/approximations/InverseMultiquadric.hpp"

/// @cond
template class mm::InverseMultiquadric<double>;
template std::ostream& mm::operator<<(std::ostream& os, const mm::InverseMultiquadric<double>&);
/// @endcond
