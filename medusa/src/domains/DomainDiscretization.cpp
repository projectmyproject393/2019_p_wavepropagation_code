#include <medusa/bits/domains/DomainDiscretization.hpp>
#include <medusa/bits/types/Vec.hpp>

/**
 * @file
 * Instantiations for common domain discretizations.
 */

template class mm::DomainDiscretization<mm::Vec1d>;
template class mm::DomainDiscretization<mm::Vec2d>;
template class mm::DomainDiscretization<mm::Vec3d>;
