add_library(domains
        ../../include/medusa/bits/domains/DomainDiscretization_fwd.hpp
        ../../include/medusa/bits/domains/DomainDiscretization.hpp
        ../../include/medusa/bits/domains/discretization_helpers.hpp
        DomainDiscretization.cpp
        ../../include/medusa/bits/domains/DomainShape_fwd.hpp
        ../../include/medusa/bits/domains/DomainShape.hpp
        DomainShape.cpp
        ../../include/medusa/bits/domains/BoxShape_fwd.hpp
        ../../include/medusa/bits/domains/BoxShape.hpp
        BoxShape.cpp
        ../../include/medusa/bits/domains/UnknownShape_fwd.hpp
        ../../include/medusa/bits/domains/UnknownShape.hpp
        UnknownShape.cpp
        ../../include/medusa/bits/domains/CircleShape_fwd.hpp
        ../../include/medusa/bits/domains/CircleShape.hpp
        CircleShape.cpp
        ../../include/medusa/bits/domains/PolygonShape_fwd.hpp
        ../../include/medusa/bits/domains/PolygonShape.hpp
        PolygonShape.cpp
        ../../include/medusa/bits/domains/ShapeUnion_fwd.hpp
        ../../include/medusa/bits/domains/ShapeUnion.hpp
        ShapeUnion.cpp
        ../../include/medusa/bits/domains/ShapeDifference_fwd.hpp
        ../../include/medusa/bits/domains/ShapeDifference.hpp
        ShapeDifference.cpp)
target_link_libraries(domains assert types memutils numutils kdtree)
register_library(domains)

add_library(domain_engines
        ../../include/medusa/bits/domains/FillEngine.hpp
        ../../include/medusa/bits/domains/PoissonDiskSampling_fwd.hpp
        ../../include/medusa/bits/domains/PoissonDiskSampling.hpp
        PoissonDiskSampling.cpp
        ../../include/medusa/bits/domains/BasicRelax_fwd.hpp
        ../../include/medusa/bits/domains/BasicRelax.hpp
        BasicRelax.cpp
        ../../include/medusa/bits/domains/FindBalancedSupport_fwd.hpp
        ../../include/medusa/bits/domains/FindBalancedSupport.hpp
        FindBalancedSupport.cpp
        ../../include/medusa/bits/domains/FindClosest_fwd.hpp
        ../../include/medusa/bits/domains/FindClosest.hpp
        FindClosest.cpp)

target_link_libraries(domain_engines assert types kdtree domains)
register_library(domain_engines)