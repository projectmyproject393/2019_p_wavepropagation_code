#include <medusa/bits/types/Range.hpp>
#include "medusa/bits/domains/FindClosest.hpp"

/**
 * @file
 * Implementation of FindClosest.
 */

namespace mm {

FindClosest::FindClosest(int support_size) :
        support_size(support_size), for_which_(), search_among_(), force_self_(false) {}

FindClosest& FindClosest::forNodes(indexes_t for_which) {
    for_which_ = std::move(for_which);
    return *this;
}

FindClosest& FindClosest::searchAmong(indexes_t search_among) {
    search_among_ = std::move(search_among);
    return *this;
}

FindClosest& FindClosest::forceSelf(bool b) {
    force_self_ = b;
    return *this;
}

FindClosest& FindClosest::numClosest(int num) {
    support_size = num;
    return *this;
}

}  // namespace mm
