#include <medusa/bits/domains/CircleShape.hpp>

/**
 * @file
 * Instantiation of class for ball shaped domains.
 */

template class mm::CircleShape<mm::Vec1d>;
template class mm::CircleShape<mm::Vec2d>;
template class mm::CircleShape<mm::Vec3d>;
