# Implementation of methods present in the "RBF-FD analysis of 2D time-domain acoustic wave propagation in heterogeneous Earth’s subsurface" paper.

To compile and run the programs:

`cmake` executable and `hdf5` C headers and libraries are required to be installed on the computer before the following instructions apply.

Clone this repository and run the standard build steps:

```
mkdir build
cd build
cmake ..
```

After the process is completed the programs for each test case from the article can be compiled using
`make caseN` where `N` stands for the case number, either 1, 2 or 3.

To run the case program, navigate to the `bin/` directory (`cd ../bin/`) and run the command `./caseN ../config_case_N.xml`.
Cases can take a few minutes to finish and generate around 15GB of data.

The `config_case_N.xml` files contain the parameters which can be manipulated without recompilation.

To view the generated results open the `post_process/caseN` directory in Matlab. To plot case `N`, first run the `process.m` script and then the `plot.m` script.

