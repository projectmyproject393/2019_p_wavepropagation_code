#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <cmath>

using namespace mm;
using namespace Eigen;

const double pi = 3.14159265358979323846;

VectorXd ricker(double f, double dt) {
    double nw_ = 2.2 / f / dt;
    int nw = 2 * std::floor(nw_ / 2) + 1;
    int nc = std::floor(nw/2);
    VectorXd w = VectorXd(nw);
    VectorXd k = VectorXd(nw);
    VectorXd alpha = VectorXd(nw);
    VectorXd beta = VectorXd(nw);

    for (int i = 0; i < nw; ++i) {
        k[i]= i+1;
        alpha[i] = (nc-k[i]+1)*f*dt*pi;
        beta[i] = alpha[i]*alpha[i];
        w[i] = (1.0 - beta[i]*2)*exp(-beta[i]);
    }
    return w;
}

double distance(Vec2d coord, Vec2d target){
    return std::sqrt(ipow(coord[0] - target[0], 2) + ipow(coord[1] - target[1], 2));
}

double delta (Vec2d p, Vec2d target) {
    double eps = 4.0;
    double dis = distance(p, target);
    return 1 / pi * eps / (dis * dis + eps * eps);
}

double damping(double x, double step,int i_max){

    return ipow( std::exp( -ipow(0.015*(i_max - x/(step) -1), 2) ) ,10);
}

double suggestedStep(Vec2d p, double interval, double pointsWave, double s, double x_size, double z_size) {
    double L = 6000 * interval;
    double suggested_step = L /(3.0 * pointsWave);
    return suggested_step;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        print_red("Supply parameter file as the second argument.\n");
        return 1;
    }
    XML conf(argv[1]);
    int i_max =20;
    double size =conf.get<double>("domain.size");
    double step = conf.get<double>("domain.step");
    double v0 = conf.get<double>("problem.v");
    int n = conf.get<int>("mls.n");  // support size
    int m = conf.get<int>("mls.m");  // monomial basis of second order, i.e. 6 monomials
    int fill_seed = conf.get<int>("fill.seed");
    double init_heat = conf.get<double>("relax.init_heat");
    double final_heat = conf.get<double>("relax.init_heat");
    double num_neighbours = conf.get<double>("relax.num_neighbours");
    double riter = conf.get<double>("relax.riter");
    double sigma = conf.get<double>("mls.sigma");



    double dt_orig = conf.get<double>("problem.dt_orig");
    double interval = (72) * dt_orig; // 73 - 1
    int t_steps = conf.get<int>("problem.time_steps");
    VectorXd wave_original = ricker(0.03 / interval, interval);

    // SOURCE INTERPOLATION
    VectorXd wave = wave_original;

    // SOURCE LOCATION
    Vec2d target({150, floor(size - 1) - 150});

    // Prepare domain
    BoxShape<Vec2d> box(-1.0, size);

    // Fill with nodes
    std::cout <<"Domain fill\n";
    DomainDiscretization<Vec2d> domain = box.discretizeBoundaryWithStep(step); //
    PoissonDiskSampling<Vec2d> fill; fill.seed(fill_seed).initialPoint(target);
    domain.fill(fill, step);

    // Do relaxation of nodes
    std::cout << "Domain relax\n";
    BasicRelax relax;
    relax.initialHeat(init_heat).finalHeat(final_heat).numNeighbours(num_neighbours).iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT);
    domain.relax(relax, step);

    // Find support nodes
    std::cout << "Find support\n";
    domain.findSupport(FindClosest(n));

    // SIZE AND LABELS
    int domain_size = domain.size();
    std::cout << "Number of nodes is: " << domain_size << std::endl;
    Range<int> interior = domain.types() > 0;
    Range<int> boundary = domain.types() < 0;

    // Create output file
    std::string hdf_out_filename = conf.get<std::string>("output.path");
    HDF hdf_out(hdf_out_filename, HDF::DESTROY);
    hdf_out.writeDomain("domain", domain);


    // Compute shapes
    std::cout << "Compute shapes\n";
    WLS<Gaussians <Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest>
            approx({m,sigma} ,1);
    auto storage = domain.computeShapes<sh::lap>(approx); //Approximations (shape functions) are computed.
    auto op = storage.explicitOperators();  // operators constructed

    // Prepare for time stepping
    VectorXd T0 = VectorXd::Zero(domain_size); //0-th step
    VectorXd T1 = VectorXd::Zero(domain_size); //1-st step
    VectorXd T2 = VectorXd::Zero(domain_size); //2-nd step

    int tt;
    int t_save=1;
    Range<Vector2d> out_pos;
    for (int i : interior) {
        out_pos.push_back(domain.pos(i));
    }
    hdf_out.openGroup("/");
    hdf_out.writeDouble2DArray("pos", out_pos);
    hdf_out.openGroup("/step" + std::to_string(t_save));
    hdf_out.writeDoubleAttribute("TimeStep", t_save);
    hdf_out.writeDoubleAttribute("time", 0.0);
    VectorXd TI1 = VectorXd::Zero(domain.interior().size()); //1-st step: interior
    int j=0;
    for (int i : interior) {
        TI1[j] = T1[i];
        j++;
    }
    hdf_out.writeDoubleArray("E", TI1);
    ++t_save;

    // Time stepping
    std::cout << "Begin time stepping\n";
    for (tt = 1; tt <= t_steps; ++tt) {
        std::cout << "Time step " << tt << " of " << t_steps << std::endl;
        //solve
        for (int j = 0; j < interior.size(); ++j) {
            int i = interior[j];
            T2[i] = 2* T1[i] - T0[i] + dt_orig * dt_orig * ipow(v0, 2) * op.lap(T1, i);

        }

        //  Update source
        if (tt < wave.size()-1) {
            for (int j = 0; j < interior.size(); ++j) {
                int i = interior[j];
                T2[i] += dt_orig*dt_orig*1e8*delta(domain.pos(i), target) * wave(tt);
            }
        }

        // Time advance
        T0=T1;
        T1=T2;

        //Absorbing boundary
        step=1;
        for (int j = 0; j < interior.size(); ++j) {
            int i = interior[j];
            double x = domain.pos(i, 0);
            double z = domain.pos(i, 1);
            if (x <= (i_max-1)*step){
                T0[i]*=damping(x,step,i_max);
                T1[i]*=damping(x,step,i_max);
            } else {
                if  (x >= ((size-1)-(i_max-1)*step) ){
                    T0[i]*=damping((size-1)-x, step, i_max);
                    T1[i]*=damping((size-1)-x, step, i_max);
                } else {
                    if (z < (i_max-1)*step){
                        T0[i]*=damping(z,step,i_max);;
                        T1[i]*=damping(z,step,i_max);;
                    }
                }
            }
        }
        if ((tt+1)%10==0){ //change condition when not saving every step
            hdf_out.openGroup("/step" + std::to_string(t_save));
            hdf_out.writeDoubleAttribute("TimeStep", t_save);
            j=0;
            for (int i : interior) {
                TI1[j]= T1[i];
                j++;
            }
            hdf_out.writeDoubleArray("E", TI1);
            hdf_out.writeDoubleAttribute("time", dt_orig * tt);
        }

        ++t_save;

    }
    hdf_out.openGroup("/");
    hdf_out.openGroup("/params");
    hdf_out.writeDoubleAttribute("N", domain_size);
    hdf_out.writeDoubleAttribute("dt", dt_orig);
    hdf_out.writeDoubleAttribute("size", size);
    hdf_out.writeDoubleAttribute("step", step);
    hdf_out.writeDoubleAttribute("t_steps", t_steps);
    hdf_out.writeDoubleAttribute("n", n);
    hdf_out.writeDoubleAttribute("m", m);
    hdf_out.writeDoubleAttribute("sigma", sigma);
}
