load data3.mat;
ax(1)=subplot(2,2,1);
imagesc(MM210)
xlabel('t= 0.2s');
ax(2)=subplot(2,2,2);
imagesc(MM510)
xlabel('t= 0.5s');
ax(3)=subplot(2,2,3);
imagesc(MM710)

xlabel('t= 0.7s');
ax(4)=subplot(2,2,4);
imagesc(MM910)
xlabel('t= 0.9s');
h=colorbar;
set(h, 'Position', [.8955 .11 .0581 .8150])
for i=1:4
      pos=get(ax(i), 'Position');
      set(ax(i), 'Position', [pos(1) pos(2) 0.85*pos(3) pos(4)]);
end