
pause on;
fn = '../../bin/data3.h5'
pos = h5read(fn,'/pos');

norm0 = zeros(1,10);

% target grid
X = linspace(0,10399,800);
Y = linspace(0,3305,400);
[X, Y] = meshgrid(X, Y);

is=1;
sez = [210 510 710 910];
for j = 1:4
   i=sez(j)
   disp(['step = ',num2str(i),' of ',num2str(910)])
   num = int2str(i);
   num_mns1 = int2str(i-1);
   eval(['amp',num,'=','h5read(''',fn,''',','''/step',num,'/E''',');']); 
   F = scatteredInterpolant(pos(:,1), 3305 - pos(:,2),eval(['amp',num]));
   V1 = F(X, Y);

  
   if i==510
       MM510 = V1;

   end
   
   if i==210
       MM210 = V1;

   end
   
   if i==710
       MM710 = V1;

   end
   
   if i==910
       MM910 = V1;

   end
is = is+1;
end


save('data3.mat', 'MM510', 'MM210', 'MM710','MM910')

