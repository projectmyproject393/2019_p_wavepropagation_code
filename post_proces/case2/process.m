clear workspace;
fn='../../bin/data2.h5';
pos = h5read(fn,'/pos');

norm0 = zeros(1,10);

% target grid
X = linspace(0,499,500);
Y = linspace(0,499,500);
[X, Y] = meshgrid(X, Y);
dt1 = h5readatt(fn, '/params', 'dt');
disp(dt1)
% is = 1;
sez = [0.03, 0.07, 0.09, 0.13];
for t = sez
%    [garbage, len] = size(sez);
   disp(['step = ',num2str(t),' of ',num2str(sez(end))])
   
   [i_down, i_up] = find_up_down(dt1, t);
   disp(i_down)
   disp(i_up)
   num_down = int2str(i_down); %
   num_up =int2str(i_up);
   eval(['amp',num_down,'=','h5read(''',fn,''',','''/step',num_down,'/E''',');']); 
   F = scatteredInterpolant(pos(:,1), 499 - pos(:,2),eval(['amp',num_down]));
   V1_down = F(X, Y);
   eval(['amp',num_up,'=','h5read(''',fn,''',','''/step',num_up,'/E''',');']); 
   F = scatteredInterpolant(pos(:,1), 499 - pos(:,2),eval(['amp',num_up]));
   V1_up = F(X, Y);
   V1 = time_average(dt1, t, (i_down-1)*dt1,V1_down, V1_up);
   if t==0.03
       MM110 = V1;

   end
   
   if t==0.07
       MM210 = V1;

   end
   
   if t==0.09
       MM410 = V1;
   end
   
   if t==0.13
       MM610 = V1;

   end
   
end

save('data2.mat', 'MM110', 'MM210', 'MM410','MM610')
