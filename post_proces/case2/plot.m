load data2.mat;

cmap = gray(256);
colormap(cmap);
ax(1)=subplot(2,2,1);
imagesc(MM110)
caxis manual;
caxis([-1 1]);
xlabel('t = 0.03 s');
ax(2)=subplot(2,2,2);
imagesc(MM210)
caxis manual;
caxis([-1 1]);
xlabel('t = 0.07 s');
ax(3)=subplot(2,2,3);
imagesc(MM410)
caxis manual;
caxis([-1 1]);
xlabel('t = 0.09 s');
ax(4)=subplot(2,2,4);
imagesc(MM610)
caxis manual;
caxis([-1 1]);
xlabel('t = 0.13 s');
h=colorbar;
set(h, 'Position', [.8955 .11 .0581 .8150])
for i=1:4
      pos=get(ax(i), 'Position');
      set(ax(i), 'Position', [pos(1) pos(2) 0.85*pos(3) pos(4)]);
end