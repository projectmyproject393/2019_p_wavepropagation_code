close all
fn='../../bin/data1.h5';
pos = h5read(fn,'/pos');

norm0 = zeros(1,10);

% target grid
X = linspace(0,499,500);
Y = linspace(0,499,500);
[X, Y] = meshgrid(X, Y);

is = 1;
sez = [110 210 310 910];
for i = sez
   disp(['step = ',num2str(i),' of ',num2str(910)])
   num = int2str(i);
   num_mns1 = int2str(i-1);
   eval(['amp',num,'=','h5read(''',fn,''',','''/step',num,'/E''',');']); 
   F = scatteredInterpolant(pos(:,1), 499 - pos(:,2),eval(['amp',num]));
   V1 = F(X, Y);
   
   if i==110
       MM110 = V1;
   end
   if i==210
       MM210 = V1;
   end
   if i==310
       MM310 = V1;
   end
   if i==910
       MM910 = V1;
   end
   is = is +1;
end

save('data1.mat','MM110', 'MM210', 'MM310', 'MM910')

