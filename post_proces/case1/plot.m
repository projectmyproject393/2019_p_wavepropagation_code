load data1.mat;

cmap = gray(256);
colormap(cmap);
ax(1)=subplot(2,2,1);
imagesc(MM110)
caxis manual;
caxis([-0.5 0.5]);
xlabel('t=0.01');
ax(2)=subplot(2,2,2);
imagesc(MM210)
caxis manual;
caxis([-0.5 0.5]);
xlabel('t=0.02');
ax(3)=subplot(2,2,3);
imagesc(MM310)
caxis manual;
caxis([-0.5 0.5]);
xlabel('t=0.03');
ax(4)=subplot(2,2,4);
imagesc(MM910)
caxis manual;
caxis([-0.5 0.5]);
xlabel('t=0.9');
h=colorbar;
set(h, 'Position', [.8955 .11 .0581 .8150])
for i=1:4
      pos=get(ax(i), 'Position');
      set(ax(i), 'Position', [pos(1) pos(2) 0.85*pos(3) pos(4)]);
end
set(findobj(gcf,'type','axes'),'FontName','Helvetica','FontSize',15,'FontWeight','Bold', 'LineWidth', 2);
